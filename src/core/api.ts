import * as pako from "pako";
import Config from "./config";

type MethodType = ('GET'|'POST'|'PATCH'|'PUT'|'DELETE');

export const apiFetch = async (
  uri: string,
  body: BodyInit = null,
  method: MethodType = 'GET',
  gzip: boolean = true
) => {
  let headers = {};
  headers['Accept'] = 'application/json';
  console.log(body);
  if (!(body instanceof FormData)) {
    if (body instanceof File) {
      // no content type
    } else {
      headers['Content-Type'] = 'application/json';
    }
    if (body && (body instanceof String) && gzip) {
      if (process.env.NODE_ENV !== 'production') {
        console.log(body.substring(0, 500));
      }
      headers['Content-Encoding'] = 'gzip';
      body = pako.gzip(body);
    }
  }

  const response = await fetch(Config.apiUrl + uri, {
    credentials: "include",
    headers: headers,
    method: method,
    body: body,
  }),
  json = await response.json();
  if (response.status < 200 || response.status > 300) {
    return Promise.reject(json);
  }
  return json;
},
apiGet = async (uri, params: Object = null, all: boolean = false) => {
    let q = uri
        + (params ? (uri.indexOf('?') === -1 ? '?' : '&') + getQueryString(params) : '');
    q = q +  (all ? (q.indexOf('?') === -1 ? '?' : '&') + 'all=1' : '');
    console.log(q);
    return await apiFetch(q);
},
apiPost = async (uri, body, gzip = false) => {
    return await apiFetch(uri, body, 'POST', gzip);
},
apiPatch = async (uri, body, gzip = false) => {
    return await apiFetch(uri, body, 'PATCH', gzip);
},
apiPut = async (uri, body, gzip = false) => {
    return await apiFetch(uri, body, 'PUT', gzip);
},
apiDelete = async (uri) => {
    return await apiFetch(uri, null, 'DELETE', false);
};

function getQueryString(params) {
  return Object
  .keys(params)
  .map(k => {
      if (Array.isArray(params[k])) {
          return params[k]
              .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
              .join('&')
      }

      return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
  })
  .join('&')
}
