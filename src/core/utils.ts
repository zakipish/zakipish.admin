export abstract class AssignableObject {
  constructor(data?: Object) {
    if (data) {
      Object.assign(this, data);
    }
  }
}