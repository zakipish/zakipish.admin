import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router";

import { history } from "./history";

const initialState = {};

const initial = (state = {}, action) => {
  return state;
};

const reducers = combineReducers({
  initial
});

const enhancer = compose(
  applyMiddleware(
    routerMiddleware(history) // for dispatching history actions
  )
);

export const store = createStore(reducers, initialState, enhancer);
