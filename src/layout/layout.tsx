import * as React from "react";
import { withStyles, Theme, StyleRulesCallback } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

import Header from "./header";
import Sidebar from "./sidebar";

import { Router } from "./router";
import styles from "./styles";
import { apiGet } from "../core/api";
import Login from "./login";
import Notification from "../components/notification/notification";

interface LayoutState {
  mobileOpen: boolean,
};

interface LayoutProps {
  classes: any,
  theme: Theme,
};

class Layout extends React.Component<LayoutProps> {

  mounted = false;
  unmounted = false;

  state = {
    loading: true,
    authorized: false,
    mobileOpen: false,
  };

  constructor(props) {
    super(props);
    this.bootstrap();
  }

  bootstrap = async () => {
    try {
      const user = await apiGet('/auth');
      this.setStateObj({
        user,
        authorized: true,
      });
    } catch {
    }
    this.setStateObj({
      loading: false,
    });
  };

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  setStateObj = (newState) => {
    if (this.unmounted) {
      return;
    }
    if (this.mounted) {
      this.setState(newState);
    } else {
      this.state = {...this.state, ...newState};
    }
  };

  handleDrawerToggle = () => {
    this.setState((state: LayoutState) => ({ mobileOpen: !state.mobileOpen }));
  };

  onLogin = (user: any) => {
    this.setState({
      authorized: true,
      user
    })
  }

  render () {
    const { classes, theme } = this.props;
    const { mobileOpen, authorized, loading } = this.state
    
    if (loading) {
      return (
        <CircularProgress />
      );
    }

    return (
      <div className={classes.root}>
        <Header 
          classes={classes} 
          handleDrawerToggle={this.handleDrawerToggle} 
        />
        {authorized ? (
          <React.Fragment>
            <Sidebar 
              classes={classes} 
              theme={theme} 
              handleDrawerToggle={this.handleDrawerToggle} 
              mobileOpen={mobileOpen} 
            />
            <main className={classes.content}>
              <Notification title="Сохранено"/>
              <div className={classes.toolbar} />
              <Router />
            </main>
          </React.Fragment>
        ) : (
          <Login onLogin={this.onLogin}/>
        )}
      </div>
    );
  }
};

export default withStyles(styles, { withTheme: true })(Layout);
