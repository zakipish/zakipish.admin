import * as React from "react";

import { Router as BaseRouter, Switch, Route } from "react-router-dom";

import * as Clubs from "../containers/clubs";
import * as ClubChains from "../containers/club-chains";
import * as Playgrounds from "../containers/playgrounds";
import * as PlaygroundSchedules from "../containers/playground-schedules";
import * as Services from "../containers/services";
import * as Cities from "../containers/cities";
import * as Departments from "../containers/departments";
import * as Equipments from "../containers/equipments";
import * as Surfaces from "../containers/surfaces";
import * as Infrastructure from "../containers/infrastructures";
import * as Sports from "../containers/sports";
import * as Games from "../containers/games";
import * as Events from "../containers/events";
import * as Users from "../containers/users";
import { Dashboard } from "../containers/dashboard/main";

export const Router: React.StatelessComponent<{}> = () => {
  return (
      <Switch>
        <Route exact path="/" component={Dashboard} />

        <Route exact path="/club-chains" component={ClubChains.List} />
        <Route path="/club-chains/create" component={ClubChains.Create} />
        <Route path="/club-chains/:clubChain/edit" component={ClubChains.Edit} />

        <Route exact path="/clubs" component={Clubs.List} />
        <Route path="/clubs/create" component={Clubs.Create} />
        <Route path="/clubs/:club/edit" component={Clubs.Edit} />
        <Route path="/clubs/:club/playgrounds/create" component={Playgrounds.Create} />
        <Route path="/clubs/:club/playgrounds/:playground/edit" component={Playgrounds.Edit} />
        <Route path="/clubs/:club/playgrounds/:playground/playground-schedules" component={PlaygroundSchedules.List} />
        <Route path="/clubs/:club/playgrounds/:playground/playground-schedule/create" component={PlaygroundSchedules.Create} />
        <Route path="/clubs/:club/playgrounds/:playground/playground-schedule/:schedule/edit" component={PlaygroundSchedules.Edit} />

        <Route exact path="/services" component={Services.List} />
        <Route path="/services/create" component={Services.Create} />
        <Route path="/services/:service/edit" component={Services.Edit} />

        <Route exact path="/equipments" component={Equipments.List} />
        <Route path="/equipments/create" component={Equipments.Create} />
        <Route path="/equipments/:equipment/edit" component={Equipments.Edit} />

        <Route exact path="/surfaces" component={Surfaces.List} />
        <Route path="/surfaces/create" component={Surfaces.Create} />
        <Route path="/surfaces/:surface/edit" component={Surfaces.Edit} />

        <Route exact path="/infrastructures" component={Infrastructure.List} />
        <Route path="/infrastructures/create" component={Infrastructure.Create} />
        <Route path="/infrastructures/:infrastructure/edit" component={Infrastructure.Edit} />

        <Route exact path="/cities" component={Cities.List} />
        <Route path="/cities/create" component={Cities.Create} />
        <Route path="/cities/:city/edit" component={Cities.Edit} />
        <Route path="/cities/:city/departments/create" component={Departments.Create} />
        <Route path="/cities/:city/departments/:department/edit" component={Departments.Edit} />

        <Route exact path="/sports" component={Sports.List} />
        <Route path="/sports/:sport/edit" component={Sports.Edit} />

        <Route exact path="/games" component={Games.List} />
        <Route path="/games/create" component={Games.Create} />
        <Route path="/games/:game/edit" component={Games.Edit} />

        <Route exact path="/events" component={Events.List} />
        <Route path="/events/create" component={Events.Create} />
        <Route path="/events/:event/edit" component={Events.Edit} />

        <Route exact path="/users-roles" component={Users.Roles} />
        <Route path="/users-roles/:role" component={Users.List} />
        <Route path="/users/:user/edit" component={Users.Edit} />
        <Route path="/users/create/:role" component={Users.Create} />
      </Switch>
  );
};
import {Paper} from "@material-ui/core";
