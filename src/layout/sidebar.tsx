import * as React from "react";
import { withStyles, Theme, StyleRulesCallback } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import Drawer from '@material-ui/core/Drawer';
import SidebarMenu from './sidebar-menu';

interface WithProps {
  handleDrawerToggle: () => void;
  theme: Theme,
  mobileOpen: boolean,
  classes: any,
}

export default class Sidebar extends React.Component<WithProps> {
  render() {
    const { classes, theme, mobileOpen, handleDrawerToggle } = this.props;

    const menu = <SidebarMenu classes={classes} handleDrawerToggle={handleDrawerToggle} />;
    return (
      <React.Fragment>
        <Hidden mdUp>
            <Drawer
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
            >
              {menu}
            </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.toolbar} />
            {menu}
          </Drawer>
        </Hidden>
      </React.Fragment>
    );
  }
}