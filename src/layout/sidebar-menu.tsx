import * as React from "react";
import { withStyles, StyleRulesCallback } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {Link, NavLink} from 'react-router-dom';
import { Button, StandardProps } from "@material-ui/core";
import { ButtonProps } from "@material-ui/core/Button";
import { LocationDescriptor } from 'history';

interface WithProps {
  classes: any,
  handleDrawerToggle: () => void;
}

interface ListItemLinkProps extends ListItemProps {
  // ListItemProps and LinkProps both define an 'innerRef' property
  // which are incompatible. Therefore the props `to` and `replace` are
  // simply duplicated here.
  to: LocationDescriptor
  replace?: boolean
}

class ListItemLink extends React.PureComponent<ListItemLinkProps> {
  render() {
      return <ListItem {...this.props} component={Link}/>
  }
}

export default class SidebarMenu extends React.Component<WithProps> {
  render() {
    const { classes, handleDrawerToggle } = this.props;

    return (
      <div className={classes.menu} onClick={handleDrawerToggle}>
        <List component="nav">
          <ListItemLink button to="/club-chains">
            <ListItemText primary="Сеть клубов" />
          </ListItemLink>
          <ListItemLink button to="/clubs">
            <ListItemText primary="Клубы" />
          </ListItemLink>
          <ListItemLink button to="/games">
            <ListItemText primary="Игры" />
          </ListItemLink>
          <ListItemLink button to="/events">
            <ListItemText primary="Кипиши" />
          </ListItemLink>
          <ListItemLink button to="/users-roles">
            <ListItemText primary="Пользователи" />
          </ListItemLink>
          <Divider />
            <ListItemLink button to="/services">
            <ListItemText primary="Услуги" />
          </ListItemLink>
          <ListItemLink button to="/equipments">
            <ListItemText primary="Инвентарь" />
          </ListItemLink>
          <ListItemLink button to="/surfaces">
            <ListItemText primary="Типы покрытий" />
          </ListItemLink>
          <ListItemLink button to="/infrastructures">
            <ListItemText primary="Инфраструктура" />
          </ListItemLink>
          <ListItemLink button to="/cities">
            <ListItemText primary="Города" />
          </ListItemLink>
          <ListItemLink button to="/sports">
            <ListItemText primary="Виды спорта" />
          </ListItemLink>
          {/* <ListItemLink button to="/playgrounds">
            <ListItemText primary="Playgrounds" />
          </ListItemLink> */}
          {/* <ListItemLink button to="/sports">
            <ListItemText primary="Sports" />
          </ListItemLink> */}
        </List>
        {/* <Divider />
        <List component="nav">
          <ListItem>
            <ListItemText primary="Logout" />
          </ListItem>
        </List> */}
      </div>
    );
  }
}
