import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Theme, withStyles, StyleRulesCallback, CircularProgress } from '@material-ui/core';
import Submit from '../components/form/submit';
import { apiPost } from '../core/api';
import Error from '../components/form/error';

const styles: StyleRulesCallback = (theme:Theme) => ({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  buttonProgress: {
    // color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  }
});

class Login extends React.Component<{
  classes: any,
  onLogin: (user:any) => void,
}> {

  unmounted = false;
  
  state = {
    login: '',
    password: '',
    loading: false,
    error: null,
  }
  
  componentWillUnmount() {
    this.unmounted = true;
  }

  login = async () => {
    const { login, password } = this.state;
    const {...state} = this.state;
    this.setState({
      loading: true,
      error: null,
    });
    try {
      const data = await apiPost('/auth', JSON.stringify({
        login,
        password,
      }));
      this.props.onLogin(data);
    } catch(e) {
      state.error = e.message;
    }
    state.loading = false;
    !this.unmounted && this.setState({...state});
  };

  render () {
    const { classes } = this.props;
    const { login, password, loading, error } = this.state;

    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockIcon />
            </Avatar>
            <Typography variant="headline">Вход</Typography>
            <form className={classes.form}>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="login">Email или телефон</InputLabel>
                <Input 
                  id="login" name="login" autoComplete="login" autoFocus
                  disabled={loading}
                  value={login}
                  onChange={e => this.setState({login: e.target.value})} 
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="password">Пароль</InputLabel>
                <Input
                  name="password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  disabled={loading}
                  value={password}
                  onChange={e => this.setState({password: e.target.value})} 
                />
              </FormControl>
              {/* <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              /> */}
              {error && <Error message={ error } />}
              <Button
                type="submit"
                fullWidth
                variant="raised"
                color="primary"
                className={classes.submit}
                disabled={loading}
                onClick={this.login}
              >
                 Войти
              </Button>
              {loading && <CircularProgress size={24} className={classes.buttonProgress} color="secondary"/>}
            </form>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(Login);
