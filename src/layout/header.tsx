import * as React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { Link } from 'react-router-dom';
import { ButtonLink } from "../components/buttons";
import Button, { ButtonProps } from '@material-ui/core/Button';


interface WithProps {
  handleDrawerToggle: () => void;
  classes: any;
}

export default class Header extends React.Component<WithProps> {
  render() {
    const { classes, handleDrawerToggle } = this.props;
    return (
      <AppBar 
        className={classes.appBar}
        position="absolute"
        color="secondary"
      >
        <Toolbar>
          <IconButton 
            color="inherit" 
            aria-label="Menu"
            onClick={handleDrawerToggle}
            className={classes.navIconHide}
          >
            <MenuIcon />
          </IconButton>
          <ButtonLink to="/" color="inherit">
            <Typography variant="title" color="inherit">Fortress</Typography>
          </ButtonLink>
        </Toolbar>
      </AppBar>
    );
  }
}
