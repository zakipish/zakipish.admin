import { StyleRulesCallback } from '@material-ui/core/styles';

const drawerWidth = 240;

const styles: StyleRulesCallback = theme => ({
    root: {
      flexGrow: 1,
      height: '100%',
      zIndex: 1,
      overflow: 'hidden',
      position: 'relative',
      display: 'flex',
      width: '100%',
    },
    appBar: {
      position: 'absolute',
      zIndex: theme.zIndex.drawer + 1,
    },
    navIconHide: {
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
      minHeight: '100vh',
      [theme.breakpoints.up('md')]: {
        position: 'relative',
      },
    },
    menu: {
      // minHeight: '100%',
    },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing.unit * 3,
    },
  });
  
export default styles;