import * as React from "react";
import { Link } from "react-router-dom";
import moment from 'moment';
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Paper} from "@material-ui/core";

import {IconButtonLink, AddButton} from "../../components/buttons";
import {H1} from "../../components/titles";
import Pagination from "../../components/pagination";
import {ThinTableCell} from "../../components/table";

export class List extends React.Component<{}, {}> {
    apiUri = '/events/';


    getDateOrInfinity = (date) => {
        return date
            ? moment(date, moment.HTML5_FMT.DATETIME_LOCAL_SECONDS).format('DD.MM.YYYY HH:mm')
            : '∞'
    }

    renderDuration = (duration) => {
        if (!duration) {
            return '∞—∞';
        }
        return this.getDateOrInfinity(duration[0]) + '—' + this.getDateOrInfinity(duration[1]);
    }

    render() {
        return (
            <React.Fragment>
                <H1>
                    Кипиши
                </H1>
                <Paper>
                    <Pagination url="/events">
                        {items => (
                            <Table aria-labelledby="tableTitle">
                                <TableHead>
                                    <TableRow>
                                        <ThinTableCell>
                                        </ThinTableCell>
                                        <TableCell>Название</TableCell>
                                        <TableCell>Описание</TableCell>
                                        <TableCell>Мастерство</TableCell>
                                        <TableCell>Мин. игроков</TableCell>
                                        <TableCell>Макс. игроков</TableCell>
                                        <TableCell>Клуб</TableCell>
                                        <TableCell>Место</TableCell>
                                        <TableCell>Вид спорта</TableCell>
                                        <TableCell>Продолжительность</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {items.map(item => (
                                        <TableRow
                                            hover
                                            key={item.id}
                                        >
                                            <ThinTableCell>
                                                <IconButtonLink to={`/events/${item.id}/edit`} aria-label="Edit">
                                                    <EditIcon/>
                                                </IconButtonLink>
                                            </ThinTableCell>
                                            <TableCell>
                                                <Link to={`/events/${item.id}/edit`} aria-label="Edit">{item.title}</Link>
                                            </TableCell>
                                            <TableCell>{item.description}</TableCell>
                                            <TableCell>{item.game_level ? item.game_level.name : ''}</TableCell>
                                            <TableCell>{item.min_players}</TableCell>
                                            <TableCell>{item.max_players}</TableCell>
                                            <TableCell>{item.club && item.club.name}</TableCell>
                                            {/* TODO: место */}
                                            <TableCell>{item.playground && item.playground.name}</TableCell>
                                            <TableCell>{item.sport && item.sport.name}</TableCell>
                                            <TableCell>
                                                {moment(item.date).format('D MMM YY')}
                                                <br />
                                                {item.interval ?
                                                    `${item.interval[0]} - ${item.interval[1]}`
                                                : ''}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        )}
                    </Pagination>
                </Paper>
                <AddButton to="/events/create"/>
            </React.Fragment>
        );
    }
};
