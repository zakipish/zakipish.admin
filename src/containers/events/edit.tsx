import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { Event } from "../../models/event";
import { history } from "../../core/history";
import {H1} from "../../components/titles";

export class Edit extends React.Component<{
    match: any;
}> {
    unmounted = false;

    state = {
        item: null,
        id: null,
        loading: true,
    }

    componentDidMount() {
        this.bootstrap();
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    bootstrap = async () => {
        const { event } = this.props.match.params;
        this.setState({
            id: event,
        });
        try {
            const data = await apiGet(`/events/${event}`);
            !this.unmounted && this.setState({
                item: new Event(data.data),
                form: new Event({...data.data}),
                loading: false,
            });
        } catch (e) {
            history.push('/events');
        }
    };

    onSubmit = (saved: Event) => {
        console.log(saved);
        this.setState({
            item: new Event(saved),
        });
    };

    render() {
        const { id, loading, item } = this.state;
        return (
            <React.Fragment>
                <H1>
                    <BackButton to="/events" />
                    Кипиш №{id} {item ? `"${item.title}"` : ''}
                </H1>
                <Loader loading={loading}>
                    {item && (
                        <PaperPadded>
                            <Form item={item} onSubmitted={this.onSubmit} />
                        </PaperPadded>
                    )}
                </Loader>
            </React.Fragment>
        );
    }
}
