import * as React from "react"
import * as _ from "lodash"

import Submit from "../../components/form/submit"
import AbstractForm from "../abstract-form"
import Input from "../../components/form/input-control"
import {apiGet} from "../../core/api"
import SelectControl from "../../components/form/select-control"
import DateIntervalControl from "../../components/form/DateIntervalControl"
import DateControl from "../../components/form/DateControl"
import DebounceSelect from "../../components/form/select-debounce"
import Grid from "@material-ui/core/Grid"
import {Button, FormControl, IconButton} from "@material-ui/core"
import DeleteIcon from "@material-ui/icons/Delete"
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox/Checkbox"

import RadioGroup from '@material-ui/core/RadioGroup'
import Radio from '@material-ui/core/Radio'

import CircularProgress from '@material-ui/core/CircularProgress'
import Divider from '@material-ui/core/Divider'

import {H3} from "../../components/titles"
import { EventType, ParticipationType } from '../../models/event'
import { Image } from '../../models/image'

import DefferedUploadImage from "../../components/deffered-upload-image";

export default class Form extends AbstractForm<{}, {
    sports: any
    levels: any
    statuses: any
    disabled: any
    users: any
    search?: string
    playerStatuses: any
    cities: {
        value: string | number
        label: string
        id: number
    }[]
}> {
    apiUri = '/events/'

    constructor(props) {
        super(props)
        this.state.users = []
        this.bootstrap()
    }

    componentDidMount() {
        this.setState({ disabled: false })
    }

    bootstrap = () => {
        this.getSports()
        this.getCities()
        this.getLevels()
    }

    getCities = async () => {
        const response = await apiGet('/cities', null, true)
        !this.unmounted && this.setState({
            cities: response.data.map(el => ({ value: el.id, label: el.name, id: el.id }))
        })
    }

    getPlayerStatuses = async () => {
        const response = await apiGet(`/player-statuses`, null, true)
        !this.unmounted && this.setState({
            playerStatuses: response.data.map(el => ({ value: el.id, label: el.name, id: el.id }))
        })
    }

    getLevels = async () => {
        const response = await apiGet(`/game-levels`, null, true)
        !this.unmounted && this.setState({
            levels: response.data.map(el => ({value: el.id, label: el.name, id: el.id}))
        })
    }

    getStatuses = async () => {
        const response = await apiGet(`/game-statuses`, null, true)
        !this.unmounted && this.setState({
            statuses: response.data.map(el => ({value: el.id, label: el.name, id: el.id}))
        })
    }

    getSports = async () => {
        const response = await apiGet(`/sports`, null, true)

        //sorting by label before delimiting
        response.data = response.data.sort((a, b) => {
            return a.label.localeCompare(b.label)
        })

        //delimiting
        let delimiterCount = null
        let name = null
        ! this.unmounted && this.setState({
            sports: response.data.map(el => {
                //counting label-delimiters
                delimiterCount = el.label.split(".").length - 1

                //insert delimiters
                name = el.name
                for (let counter = 0; counter < delimiterCount; counter++) {
                    name = '--' + name
                }
                return ({
                    value: el.id,
                    label: name
                })
            }),
        })
    }

    checkErrors = (value) => {
        !this.unmounted && this.setState({disabled: value})
    }

    //handle debounce search onInputValue event
    handleDebounceSelectUsersSearch = async (value, callback) => {
        //if value exist then request
        let users = []
        if (value.length > 2) {
            const response = await apiGet(`/users`, {search: value}, true)
            users = response.data.map(el => ({
                label: (`${el.name} ${el.lastName || ''} (${el.email || '-'})`),
                value: el.id
            }))
            callback(users)
        }
        this.setState({
            users: users,
        })
    }

    //handle select in selector
    handleDebounceUsersChange = (index: number, value: any) => {
        const { form } = this.state
        form.users[index].id = value.value
        form.users[index].email = value.label
        this.setState({
            form: {...form},
        })
    }

    //handle select in selector
    handleDebounceUserParticipationType = (index: number, value: any) => {
        const { form } = this.state
        form.users[index].type = value
        this.setState({
            form: {...form},
        })
    }

    addUser = () => {
        const {form} = this.state
        form.users.push({
            email: null,
            id: null,
        })
        this.setState({
            form,
        })
    }

    deleteUser = (user) => {
        let newUsers = this.state.form.users.filter(item => item.id !== user.id)
        let newForm = this.state.form
        newForm.users = newUsers
        this.setState({
            form: newForm,
        })
    }

    handleStatusChange = (index: number, value: any) => {
        const {form} = this.state

        form.users[index].game_user.player_status_id = value
        this.setState({
            form: {...form},
        })
    }

    handleChangeImages = (images: Image[]) => {
        let { form } = this.state
        form.images = images
        console.log({ images })
        this.setState({ form })
    }

    handleChangeSport = (value: any) => {
        const { form } = this.state
        form.sport_id = value
        form.club_id = null
        this.setState({ form })
    }

    handleChangeCity = (value: any) => {
        const { form } = this.state
        form.city_id = value
        form.club_id = null
        this.setState({ form })
    }

    handleChangeClub = async (value: any) => {
        const { form } = this.state
        form.club_id = value.value
        form.club = { id: value.value, name: value.label, playground: value.playgrounds }
        this.setState({ form })
    }

    handleChangePlayground = async (value: any) => {
        const { form } = this.state
        const playground = form.club.playgrounds.find(playground => playground.id == value)
        form.playground_id = value
        form.playground = playground
        this.setState({ form })
    }

    searchClubs = async (value, callback) => {
        const { sport_id, city_id } = this.state.form
        let clubs = []
        if (value.length > 2) {
            const response = await apiGet('/clubs', { search: value, sport_id, city_id }, true)
            clubs = response.data.map(el => ({ value: el.id, label: el.name, id: el.id, playgrounds: el.playgrounds, sports: el.sports, city_id: el.city_id }))
            callback(clubs)
        }
    }

    handleChangePlaceType = async (event) => {
        const { form } = this.state
        form.place_type = event.target.value

        if (form.place_type == 'Coordinates') {
            form.playground_id = null
            form.playground = null
        } else {
            form.coordinates = null
            form.address = null
        }

        this.setState({ form })
    }

    render() {
        const { item } = this.props
        const {
            form, busy, message, sports, levels, disabled, playerStatuses, cities
        } = this.state

        return (form &&
            <form>
                <Input
                    label="ЧПУ"
                    value={form.slug}
                    change={this.handleChange('slug')}
                    margin="normal"
                    fullWidth
                    disabled={true}
                    error={this.fieldError('slug')}
                    helperText={'Генерируется автоматически'}
                />

                <SelectControl
                    suggestions={[
                        { value: EventType.Meeting, label: 'Встреча' },
                        { value: EventType.Game, label: 'Игра' }
                    ]}
                    value={form.type}
                    onChange={this.handleChange('type')}
                    label="Тип"
                    margin="normal"
                    fullWidth
                />

                <Input
                    label="Название"
                    value={form.title}
                    change={this.handleChange('title')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    errorText={this.fieldError('title')}
                />

                <Input
                    label="Мин. игроков"
                    value={form.min_players}
                    margin="normal"
                    fullWidth
                    type="number"
                    disabled={busy}
                    change={this.handleChange('min_players')}
                    errorText={this.fieldError('min_players')}
                />

                <Input
                    label="Макс. игроков"
                    value={form.max_players}
                    margin="normal"
                    type="number"
                    fullWidth
                    disabled={busy}
                    change={this.handleChange('max_players')}
                    errorText={this.fieldError('max_players')}
                />

                <Input
                    style={{marginTop: "0", marginBottom: "16px"}}
                    label="Текстовое описание"
                    value={form.description}
                    change={this.handleChange('description')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    multiline
                    rows={5}
                    errorText={this.fieldError('description')}
                />

                {levels && <SelectControl
                    suggestions={levels}
                    value={form.game_level_id}
                    label="Уровень мастерства"
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange('game_level_id')}
                    errorText={this.fieldError('game_level_id')}
                />}

                <Divider />
                <br />

                <H3>Место</H3>

                {cities && sports ? (
                    <>
                        <SelectControl
                            suggestions={cities}
                            value={form.city_id}
                            label="Город"
                            margin="normal"
                            onChange={this.handleChangeCity}
                            fullWidth
                        />

                        <SelectControl
                            suggestions={sports}
                            value={form.sport_id}
                            label="Вид спорта"
                            margin="normal"
                            fullWidth
                            onChange={this.handleChangeSport}
                            errorText={this.fieldError('sport_id')}
                        />

                        <DebounceSelect
                            label="Клуб"
                            value={{ value: form.club_id, label: form.club.name }}
                            placeholder="Начните вводить название клуба"
                            onChange={this.handleChangeClub}
                            loadOptions={this.searchClubs}
                            latitude={1200}
                            margin="normal"
                            emptyMessage="Для поиска начните вводить название клуба"
                            fullWidth
                        />

                        {form.type == EventType.Meeting && (
                            <RadioGroup
                                name="place_type"
                                value={form.place_type}
                                onChange={this.handleChangePlaceType}
                            >
                                <FormControlLabel value="Playground" control={<Radio />} label="Зал" />
                                <FormControlLabel value="Coordinates" control={<Radio />} label="Координаты" />
                            </RadioGroup>
                        )}

                        {form.type == EventType.Game || form.place_type == 'Playground' ? (
                            <SelectControl
                                suggestions={form.club && form.club.playgrounds ? form.club.playgrounds.map(playground => ({
                                    value: playground.id,
                                    label: playground.name,
                                })) : []}
                                value={form.playground_id}
                                label="Зал"
                                margin="normal"
                                fullWidth
                                onChange={this.handleChangePlayground}
                                errorText={this.fieldError('playground_id')}
                            />
                        ) : (
                            <>
                                <Input
                                    label="Координаты"
                                    value={form.coordinates}
                                    change={this.handleChange('coordinates')}
                                    fullWidth
                                    margin="normal"
                                    disabled={busy}
                                    errorText={this.fieldError('coordinates')}
                                />
                                <Input
                                    label="Адрес"
                                    value={form.address}
                                    change={this.handleChange('address')}
                                    fullWidth
                                    margin="normal"
                                    disabled={busy}
                                    errorText={this.fieldError('address')}
                                />
                            </>
                        )}
                    </>
                ) : (
                    <CircularProgress />
                )}

                <Divider />

                <DateControl
                    type='date'
                    margin="normal"
                    fullWidth
                    label="Дата"
                    value={form.date}
                    onChange={this.handleChange('date')}
                    disabled={busy}
                    errorText={this.fieldError('date')}
                />

                <FormControlLabel
                    control={
                        <Checkbox
                            checked={form.blockSchedule}
                            onChange={(e) => this.handleChange('blockSchedule')(e.target.checked)}
                        />}
                    labelPlacement="end"
                    label="Блокировать расписание"
                />

                <DateIntervalControl
                    type="time"
                    label="Время"
                    margin="normal"
                    fullWidth
                    value={form.interval}
                    onChange={this.handleChange('interval')}
                    disabled={busy}
                    errorText={this.fieldError('interval')}
                />

                <FormControl margin="normal" fullWidth>
                    <H3>Игроки</H3>
                    {form.users && form.users.map((user, i) => (
                        <Grid container spacing={8} key={i}>
                            <Grid item xs={12} md={5}>
                                <SelectControl
                                    style={{ marginTop: 17 }}
                                    suggestions={[
                                        { label: "Возможно", value: ParticipationType.Possible },
                                        { label: "Определенно", value: ParticipationType.Definitely },
                                        { label: "Отказался", value: ParticipationType.Declined },
                                    ]}
                                    label="Тип участия"
                                    value={user.type}
                                    onChange={(value) => this.handleDebounceUserParticipationType(i, value)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <DebounceSelect
                                    label="Пользователь"
                                    value={{value: user.id, label: user.id ? `${user.name} ${user.lastName || ''} (${user.email || '-'})` : ''}}
                                    placeholder="Начните вводить имя или email"
                                    onChange={(value) => this.handleDebounceUsersChange(i, value)}
                                    loadOptions={this.handleDebounceSelectUsersSearch}
                                    latitude={1200}
                                    margin="normal"
                                    emptyMessage="Для поиска начните вводить имя или email пользователя"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={1} style={{marginTop:25}}>
                                <IconButton style={{marginRight: "5px"}} aria-label="Delete"
                                    onClick={() => this.deleteUser(user)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </Grid>
                        </Grid>

                    ))}
                </FormControl>

                <Grid xs={12} item container justify="flex-end" alignItems="center">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={this.addUser}
                        style={{float: 'right'}}
                    >
                        Добавить пользователя
                    </Button>
                </Grid>

                <H3>Цена за участие</H3>
                <Grid container spacing={8}>
                    <Grid item sm={12} md={3} style={{ marginTop: 17 }}>
                        <Input
                            style={{marginTop: "0", marginBottom: "16px"}}
                            label="Цена"
                            type="number"
                            value={form.price}
                            change={this.handleChange('price')}
                            margin="normal"
                            errorText={this.fieldError('price')}
                            fullWidth
                        />
                    </Grid>
                </Grid>

                <Divider />

                <DefferedUploadImage url={'/images/multiple'} images={form.images} field="images[]" title="Фото" onChange={this.handleChangeImages} />

                <Submit
                    float
                    onSubmit={this.submit}
                    disabled={_.isEqual(form, item) || disabled}
                    loading={busy}
                    message={message}
                />
            </form>
        )
    }
}
