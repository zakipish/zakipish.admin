import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import Form from "./form";
import { Event } from "../../models/event";
import { history } from "../../core/history";

export class Create extends React.Component {
    state = {
        item: new Event(),
    };

    onSubmit = (saved: Event) => {
        history.push(`/events/${saved.id}/edit/`);
    };

    render() {
        const { item } = this.state;
        return (
            <React.Fragment>
                <H1>
                    <BackButton to="/events" />
                    Создание кипиша
                </H1>
                <Loader loading={false}>
                    <PaperPadded>
                        <Form item={item} onSubmitted={this.onSubmit} />
                    </PaperPadded>
                </Loader>
            </React.Fragment>
        );
    }
}
