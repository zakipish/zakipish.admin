import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import Form from "./form";
import { history } from "../../core/history";
import { PlaygroundType } from "../../models/playground-type";

export class Create extends React.Component {

  state = {
    item: new PlaygroundType(),
  }

  onSubmit = (saved: PlaygroundType) => {
    history.push(`/playground-types/${saved.id}/edit/`);
  };

  render() {
    const { item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/playground-types" />
          Создание типа зала
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
