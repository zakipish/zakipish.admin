import * as React from "react";

import { BackButton, AddButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1, H2 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { List, ListItem, ListItemText } from "@material-ui/core";
import { PlaygroundType } from "../../models/playground-type";
import OldSlugs from "../../components/old-slugs";
import { history } from "../../core/history";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;
  
  state = {
    item: null,
    id: null,
    loading: true,
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }

  bootstrap = async () => {
    const { playgroundType } = this.props.match.params;
    this.setState({
      id: playgroundType,
    });
    try {
      const data = await apiGet(`/playground-types/${playgroundType}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push('/playground-types');
    }
  };

  onSubmit = (saved: PlaygroundType) => {
    this.setState({
      item: new PlaygroundType(saved),
    });
    history.push('/playground-types');
  };

  render() {
    const { id, loading, item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/playground-types" />
          Тип зала №{id} {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
            <PaperPadded>
              <Form item={item} onSubmitted={this.onSubmit} />
              <OldSlugs slugs={item.slugs}></OldSlugs>
            </PaperPadded>
          )}
        </Loader>
      </React.Fragment>
    );
  }
}
