import * as React from "react";

import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper } from "@material-ui/core";

import { IconButtonLink, AddButton } from "../../components/buttons";
import { H1 } from "../../components/titles";
import Pagination from "../../components/pagination";
import { ThinTableCell } from "../../components/table";

export class List extends React.Component {
  render() {
    return (
      <React.Fragment>
        <H1>
          Типы покрытий
        </H1>
        <Paper>
          <Pagination url="/surfaces"> 
            { items => (
              <Table aria-labelledby="tableTitle">
                <TableHead>
                  <TableRow>
                    <ThinTableCell padding="dense">
                      Id
                    </ThinTableCell>
                    <TableCell>
                      Название
                    </TableCell>
                    <TableCell>
                      ЧПУ
                    </TableCell>
                    <ThinTableCell  padding="none">
                    </ThinTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map(item => (
                    <TableRow
                      hover
                      key={item.id}
                    >
                      <ThinTableCell numeric padding="dense">
                        {item.id}
                      </ThinTableCell>
                      <TableCell>
                        {item.name}
                      </TableCell>
                      <TableCell>
                        {item.slug}
                      </TableCell>
                      <ThinTableCell padding="none">
                        <IconButtonLink to={`/surfaces/${item.id}/edit`} aria-label="Редактировать">
                          <EditIcon />
                        </IconButtonLink>
                      </ThinTableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </Pagination>
        </Paper> 
        <AddButton to="/surfaces/create" />
      </React.Fragment>
    );
  }
};
