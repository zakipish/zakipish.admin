import * as React from "react";
import moment from 'moment';
import {Link} from "react-router-dom";
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Paper} from "@material-ui/core";

import {IconButtonLink, AddButton} from "../../components/buttons";
import {H1} from "../../components/titles";
import Pagination from "../../components/pagination";
import {ThinTableCell} from "../../components/table";
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';

export class List extends React.Component<{}, {}> {
    apiUri = '/games/';


    getDateOrInfinity = (date) => {
        return date
            ? moment(date, moment.HTML5_FMT.DATETIME_LOCAL_SECONDS).format('DD.MM.YYYY HH:mm')
            : '∞'
    }

    renderDuration = (duration) => {
        if (!duration) {
            return '∞—∞';
        }
        return this.getDateOrInfinity(duration[0]) + '—' + this.getDateOrInfinity(duration[1]);
    }

    render() {
        let buttonUp: React.ReactNode = <KeyboardArrowUp/>;
        let buttonDown: React.ReactNode = <KeyboardArrowDown/>;
        return (
            <React.Fragment>
                <H1>
                    Игры
                </H1>
                <Paper>
                    <Pagination url="/games">
                        {items => (
                            <Table aria-labelledby="tableTitle">
                                <TableHead>
                                    <TableRow>
                                        <ThinTableCell>
                                        </ThinTableCell>
                                        <TableCell>Название</TableCell>
                                        <TableCell>Описание</TableCell>
                                        <TableCell>Мастерство</TableCell>
                                        <TableCell>Мин. игроков</TableCell>
                                        <TableCell>Макс. игроков</TableCell>
                                        <TableCell>Площадка</TableCell>
                                        <TableCell>Вид спорта</TableCell>
                                        <TableCell>Продолжительность</TableCell>

                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {items.map(item => (
                                        <TableRow
                                            hover
                                            key={item.id}
                                        >
                                            <ThinTableCell>
                                                <IconButtonLink to={`/games/${item.id}/edit`} aria-label="Edit">
                                                    <EditIcon/>
                                                </IconButtonLink>
                                            </ThinTableCell>
                                            <TableCell>
                                                <Link to={`/games/${item.id}/edit`} aria-label="Edit">{item.name}</Link>
                                            </TableCell>
                                            <TableCell>{item.description}</TableCell>
                                            <TableCell>{item.game_level ? item.game_level.name : ''}</TableCell>
                                            <TableCell>{item.min_players}</TableCell>
                                            <TableCell>{item.max_players}</TableCell>
                                            <TableCell>{item.playground.name}</TableCell>
                                            <TableCell>{item.sport.name}</TableCell>
                                            <TableCell>
                                                {moment(item.date).format('D MMM YY')}
                                                <br />
                                                {item.interval ?
                                                    `${item.interval[0]} - ${item.interval[1]}`
                                                : ''}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        )}
                    </Pagination>
                </Paper>
                <AddButton to="/games/create"/>
            </React.Fragment>
        );
    }
};
