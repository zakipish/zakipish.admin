import * as React from "react"
import * as _ from "lodash"

import Submit from "../../components/form/submit"
import AbstractForm from "../abstract-form"
import Input from "../../components/form/input-control"
import {apiGet} from "../../core/api"
import SelectControl from "../../components/form/select-control"
import DateIntervalControl from "../../components/form/DateIntervalControl"
import DateControl from "../../components/form/DateControl"
import DebounceSelect from "../../components/form/select-debounce"
import Grid from "@material-ui/core/Grid"
import {Button, FormControl, IconButton} from "@material-ui/core"
import DeleteIcon from "@material-ui/icons/Delete"
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox/Checkbox"
import {H3} from "../../components/titles"

export default class Form extends AbstractForm<{}, {
    sports: any
    types: any
    playgrounds: any
    levels: any
    statuses: any
    disabled: any
    users: any
    search?: string
    playerStatuses: any
    activePrice: boolean
    cities: {
        value: string | number
        label: string
        id: number
    }[]
}> {
    apiUri = '/games/'


    constructor(props) {
        super(props)
        this.state.users = []
        this.bootstrap()
    }

    componentDidMount() {
        this.setState({disabled: false, activePrice:false})
    }

    bootstrap = () => {
        this.getSports()
        this.getCities()
        this.getLevels()
        //this.getStatuses()
        //this.getPlayerStatuses()
    }

    getCities = async () => {
        const response = await apiGet('/cities', null, true)
        !this.unmounted && this.setState({
            cities: response.data.map(el => ({value: el.id, label: el.name, id: el.id}))
        })
    }

    getPlayerStatuses = async () => {
        const response = await apiGet(`/player-statuses`, null, true)
        !this.unmounted && this.setState({
            playerStatuses: response.data.map(el => ({value: el.id, label: el.name, id: el.id}))
        })
    }

    getLevels = async () => {
        const response = await apiGet(`/game-levels`, null, true)
        !this.unmounted && this.setState({
            levels: response.data.map(el => ({value: el.id, label: el.name, id: el.id}))
        })
    }

    getStatuses = async () => {
        const response = await apiGet(`/game-statuses`, null, true)
        !this.unmounted && this.setState({
            statuses: response.data.map(el => ({value: el.id, label: el.name, id: el.id}))
        })
    }

    getSports = async () => {
        const response = await apiGet(`/sports`, null, true)

        //sorting by label before delimiting
        response.data = response.data.sort((a, b) => {
            return a.label.localeCompare(b.label)
        })

        //delimiting
        let delimiterCount = null
        let name = null
        ! this.unmounted && this.setState({
            sports: response.data.map(el => {
                //counting label-delimiters
                delimiterCount = el.label.split(".").length - 1

                //insert delimiters
                name = el.name
                for (let counter = 0; counter < delimiterCount; counter++) {
                    name = '--' + name
                }
                return ({
                    value: el.id,
                    label: name
                })
            }),
        })
    }

    checkErrors = (value) => {
        !this.unmounted && this.setState({disabled: value})
    }

    //handle debounce search onInputValue event
    handleDebounceSelectUsersSearch = async (value, callback) => {
        //if value exist then request
        let users = []
        if (value.length > 2) {
            const response = await apiGet(`/users`, {search: value}, true)
            users = response.data.map(el => ({
                label: (el.email),
                value: el.id
            }))
            callback(users)
        }
        this.setState({
            users: users,
        })
    }

    //handle select in selector
    handleDebounceUsersChange = (index: number, value: any) => {
        const {form} = this.state
        form.users[index].game_user.user_id = value.value
        form.users[index].email = value.label
        this.setState({
            form: {...form},
        })

    }

    addUser = () => {
        const {form} = this.state
        form.users.push({
            game_user: {
                user_id: null,
                player_status_id: null,
                is_new: true,
            },
            email: null,
        })
        this.setState({
            form,
        })
    }

    deleteUser = (user) => {
        let newUsers = this.state.form.users.filter(item => item.game_user.user_id !== user.game_user.user_id)
        let newForm = this.state.form
        newForm.users = newUsers
        this.setState({
            form: newForm,
        })
    }



    handleStatusChange = (index: number, value: any) => {
        const {form} = this.state

        form.users[index].game_user.player_status_id = value
        this.setState({
            form: {...form},
        })
        console.log('form', form)
    }

    changePriceCheckbox = (value:boolean) => {
        this.setState({
            activePrice: value,
        })
    }

    handleDebounceSelectPlaygroundsSearch = async (value, callback) => {
        const { sport_id, city_id } = this.state.form
        let playgrounds = []
        if (value.length > 2) {
            const response = await apiGet(`/playgrounds`, { search: value, sport_id, city_id }, true)
            playgrounds = response.data.map(el => ({
                label: el.name,
                value: el.id
            }))
            callback(playgrounds)
        }

        this.setState({
            playgrounds: playgrounds,
        })
    }

    handleDebouncePlaygroundsChange = (value: any) => {
        const { form } = this.state
        form.playground_id = value.value
        form.playground = {id:value.value, name:value.label}
        this.setState({
            form: {...form},
        })
    }

    handleChangeSport = (value: any) => {
        const { form } = this.state
        form.sport_id = value
        form.playground_id = null
        this.setState({ form })
    }

    handleChangeCity = (value: any) => {
        const { form } = this.state
        form.city_id = value
        form.playground_id = null
        this.setState({ form })
    }

    render() {
        const { item } = this.props
        const {
            form, busy, message, sports, levels, disabled, playerStatuses, activePrice, cities
            // statuses,
        } = this.state

        return (form &&
            <form>
                <Input
                    label="ЧПУ"
                    value={form.slug}
                    change={this.handleChange('slug')}
                    margin="normal"
                    fullWidth
                    disabled={true}
                    error={this.fieldError('slug')}
                    helperText={'Генерируется автоматически'}
                />

                <Input
                    label="Название"
                    value={form.name}
                    change={this.handleChange('name')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    errorText={this.fieldError('name')}
                />

                <Input
                    label="Мин. игроков"
                    value={form.min_players}
                    margin="normal"
                    fullWidth
                    type="number"
                    disabled={busy}
                    change={this.handleChange('min_players')}
                    errorText={this.fieldError('min_players')}
                />

                <Input
                    label="Макс. игроков"
                    value={form.max_players}
                    margin="normal"
                    type="number"
                    fullWidth
                    disabled={busy}
                    change={this.handleChange('max_players')}
                    errorText={this.fieldError('max_players')}
                />

                <Input
                    style={{marginTop: "0", marginBottom: "16px"}}
                    label="Текстовое описание"
                    value={form.description}
                    change={this.handleChange('description')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    multiline
                    rows={5}
                    errorText={this.fieldError('description')}
                />

                {cities && sports && (
                    <>
                        <SelectControl
                            suggestions={cities}
                            value={form.city_id}
                            label="Город"
                            margin="normal"
                            onChange={this.handleChangeCity}
                            fullWidth
                        />

                        <SelectControl
                            suggestions={sports}
                            value={form.sport_id}
                            label="Вид спорта"
                            margin="normal"
                            fullWidth
                            onChange={this.handleChangeSport}
                            errorText={this.fieldError('sport_id')}
                        />

                        <DebounceSelect
                            label="Площадка"
                            value={(form.playground) && { value: form.playground.id, label: form.playground.name }}
                            onChange={(value) => this.handleDebouncePlaygroundsChange(value)}
                            loadOptions={this.handleDebounceSelectPlaygroundsSearch}
                            latitude={1200}
                            margin="normal"
                            emptyMessage="Для поиска начните вводить название площадки"
                            errorText={this.fieldError('playground_id')}
                            disabled={!(form.sport_id && form.city_id)}
                            fullWidth
                        />
                    </>
                )}

                {levels && <SelectControl
                    suggestions={levels}
                    value={form.game_level_id}
                    label="Уровень мастерства"
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange('game_level_id')}
                    errorText={this.fieldError('game_level_id')}
                />}

                {/*statuses && <SelectControl
                    suggestions={statuses}
                    value={form.game_status_id}
                    label="Статус игры"
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange('game_status_id')}
                    errorText={this.fieldError('game_status_id')}
                />*/}

                <DateControl
                    type='date'
                    margin="normal"
                    fullWidth
                    label="Дата"
                    value={form.date}
                    onChange={this.handleChange('date')}
                    disabled={busy}
                    errorText={this.fieldError('date')}
                />

                <DateIntervalControl
                    type="time"
                    label="Время"
                    margin="normal"
                    fullWidth
                    value={form.interval}
                    onChange={this.handleChange('interval')}
                    disabled={busy}
                    errorText={this.fieldError('interval')}
                />

                <FormControl margin="normal" fullWidth>
                    <H3>Игроки</H3>
                    {form.users && form.users.map((user, i) => (
                        <Grid container spacing={8} key={i}>
                            <Grid item sm={12} md={3}>
                                {playerStatuses && <SelectControl
                                    suggestions={playerStatuses}
                                    value={user.game_user.player_status_id}
                                    label="Статус игрока"
                                    margin="normal"
                                    fullWidth
                                    onChange={(value) => this.handleStatusChange(i, value)}
                                    errorText={this.fieldError('users.'+i+'.game_user.player_status_id')}
                                />}
                            </Grid>

                            <Grid item xs={12} md={3}>
                                <DebounceSelect
                                    label="Пользователь"
                                    value={{value: user.game_user.user_id, label: user.email}}
                                    placeholder="Начните вводить имя или email"
                                    onChange={(value) => this.handleDebounceUsersChange(i, value)}
                                    loadOptions={this.handleDebounceSelectUsersSearch}
                                    latitude={1200}
                                    margin="normal"
                                    emptyMessage="Для поиска начните вводить имя или email пользователя"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={1} style={{marginTop:25}}>
                                <IconButton style={{marginRight: "5px"}} aria-label="Delete"
                                    onClick={() => this.deleteUser(user)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </Grid>
                        </Grid>

                    ))}
                </FormControl>

                <Grid xs={12} item container justify="flex-end" alignItems="center">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={this.addUser}
                        style={{float: 'right'}}
                    >
                        Добавить пользователя
                    </Button>
                </Grid>

                <H3>Цена на игру</H3>
                <Grid container spacing={8}>
                    <Grid item sm={12} md={3}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={activePrice}
                                    onChange={() => this.changePriceCheckbox(!activePrice)}
                                />}
                            labelPlacement='end'
                            label="Редактировать цену"
                        />
                    </Grid>
                    <Grid item sm={12} md={3}>
                        <Input
                            style={{marginTop: "0", marginBottom: "16px"}}
                            label="Цена"
                            type="number"
                            value={form.price}
                            change={this.handleChange('price')}
                            margin="normal"
                            fullWidth
                            disabled={!activePrice}
                            errorText={this.fieldError('price')}
                            helperText="Оставьте поле пустым, чтобы цена посчиталась автоматически"
                        />
                    </Grid>
                </Grid>

                <Submit
                    float
                    onSubmit={this.submit}
                    disabled={_.isEqual(form, item) || disabled}
                    loading={busy}
                    message={message}
                />
            </form>
        )
    }
}
