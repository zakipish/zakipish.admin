import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1, H2 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { Game } from "../../models/game";
import { ListForClub } from "../playgrounds/list-for-club";
import OldSlugs from "../../components/old-slugs";
import { history } from "../../core/history";
import UploadImage from "../../components/upload-image";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;

  state = {
    item: null,
    id: null,
    loading: true,
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }

  bootstrap = async () => {
    const { game } = this.props.match.params;
    this.setState({
      id: game,
    });
    try {
      const data = await apiGet(`/games/${game}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push('/games');
    }
  };

  onSubmit = (saved: Game) => {
    console.log(saved);
    this.setState({
      item: new Game(saved),
    });
  };

  render() {
    const { id, loading, item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/games" />
          Игра №{id} {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {console.log(item)}
          {item && (
            <PaperPadded>
              <Form item={item} onSubmitted={this.onSubmit} />
              {/*<OldSlugs slugs={item.slugs}></OldSlugs>*/}
            </PaperPadded>
          )}
        </Loader>
      </React.Fragment>
    );
  }
}
