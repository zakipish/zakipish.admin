import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import Form from "./form";
import { Game } from "../../models/game";
import { history } from "../../core/history";

export class Create extends React.Component {

  state = {
    item: new Game(),
  };

  onSubmit = (saved: Game) => {
    history.push(`/games/${saved.id}/edit/`);
  };

  render() {
    const { item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/games" />
          Создание игры
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
