import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { ClubChain } from "../../models/club-chain";
import { history } from "../../core/history";

export class Create extends React.Component {

  state = {
    item: new ClubChain(),
  }

  onSubmit = (saved: ClubChain) => {
    history.push(`/club-chains/${saved.id}/edit/`);
  };

  render() {
    const { item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/club-chains" />
          Создание сети клубов
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
