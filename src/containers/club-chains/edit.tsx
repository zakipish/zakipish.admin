import * as React from "react";

import { BackButton, AddButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1, H2 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { List, ListItem, ListItemText } from "@material-ui/core";
import { ClubChain } from "../../models/club-chain";
import OldSlugs from "../../components/old-slugs";
import { history } from "../../core/history";
import UploadImage from "../../components/upload-image";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;
  
  state = {
    item: null,
    id: null,
    loading: true,
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }

  bootstrap = async () => {
    const { clubChain } = this.props.match.params;
    this.setState({
      id: clubChain,
    });
    try {
      const data = await apiGet(`/club-chains/${clubChain}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push('/club-chains');
    }
  };

  onSubmit = (saved: ClubChain) => {
    this.setState({
      item: new ClubChain(saved),
    });
  };

  render() {
    const { id, loading, item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/club-chains" />
          Сеть клубов №{id} {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
            <PaperPadded>
              <Form item={item} onSubmitted={this.onSubmit} />
              <OldSlugs slugs={item.slugs}></OldSlugs>
            </PaperPadded>
          )}
        </Loader>
        {item && <UploadImage url={`/clubs/${item.id}`} image={item.logo} field="logo" title="Логотип"/>}
      </React.Fragment>
    );
  }
}
