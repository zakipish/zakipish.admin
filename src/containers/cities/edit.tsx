import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1, H2 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import {ListForCity} from "../departments/list-for-city";
import OldSlugs from "../../components/old-slugs";
import { history } from "../../core/history";
import { City } from "../../models/city";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;
  
  state = {
    item: null,
    id: null,
    loading: true,
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }

  bootstrap = async () => {
    const { city } = this.props.match.params;
    this.setState({
      id: city,
    });
    try {

      const data = await apiGet(`/cities/${city}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push('/cities');
    }
  };

  onSubmit = (saved: City) => {
    this.setState({
      item: new City(saved),
    });
    history.push('/cities');
  };

  render() {
    const { id, loading, item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/cities" />
          Город {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
              <div>
            <PaperPadded>
              <Form item={item} onSubmitted={this.onSubmit} />
              <OldSlugs slugs={item.slugs}></OldSlugs>
            </PaperPadded>
            <ListForCity cityId={item.id}/>
              </div>
          )}

        </Loader>
      </React.Fragment>
    );
  }
}
