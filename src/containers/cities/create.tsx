import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { history } from "../../core/history";
import { City } from "../../models/city";

export class Create extends React.Component {

  state = {
    item: new City(),
  }

  onSubmit = (saved: City) => {
    history.push(`/cities/${saved.id}/edit/`);
  };

  render() {
    const { item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/cities" />
          Создание Города
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
