import * as React from "react";

import {BackButton} from "../../components/buttons";
import Loader from "../../components/loader";
import {H1, H2, H3} from "../../components/titles";
import {PaperPadded} from "../../components/paper";
import {apiGet} from "../../core/api";
import Form from "./form";
import {Club} from "../../models/club";
import {ListForClub} from "../playgrounds/list-for-club";
import OldSlugs from "../../components/old-slugs";
import {history} from "../../core/history";
import UploadImage from "../../components/upload-image";
import Grid from "@material-ui/core/Grid/Grid";

export class Edit extends React.Component<{
    match: any;
}> {
    unmounted = false;

    state = {
        item: null,
        id: null,
        loading: true,
        form: null,
    }

    componentDidMount() {
        this.bootstrap();
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    bootstrap = async () => {
        const {sport} = this.props.match.params;
        this.setState({
            id: sport,
        });
        try {
            const data = await apiGet(`/sports/${sport}`);
            !this.unmounted && this.setState({
                item: data.data,
                form: {...data.data},
                loading: false,
            });
        } catch (e) {
            history.push('/sports');
        }
    };

    onSubmit = (saved: Club) => {
        console.log(saved);
        this.setState({
            item: new Club(saved),
        });
    };


    render() {
        const {id, loading, item} = this.state;
        return (
            <React.Fragment>
                <H1>
                    <BackButton to="/sports"/>
                   {item ? `${item.name}` : ''}
                </H1>
                <Loader loading={loading}>
                    {item && (
                        <Grid>
                            <Form item={item} onSubmitted={this.onSubmit}/>
                        </Grid>


                    )}
                </Loader>

                {item && <UploadImage url={`/sports/${item.id}`} image={item.icon} field="icon" title="Логотип" delete_action="delete_logo" />}
                {item && <UploadImage url={`/sports/${item.id}`} image={item.photo} field="photo" title="Изображние спорта в слайдере-гармошке" delete_action="delete_photo"/>}

            </React.Fragment>
        );
    }
}
