import * as React from "react";
import * as _ from "lodash";

import Submit from "../../components/form/submit";
import Input from "../../components/form/input-control";
import AbstractForm from "../abstract-form";
import UploadImage from "../../components/upload-image";

export default class Form extends AbstractForm
{

  apiUri = '/sports/';

  render() {
    const { item } = this.props;
    const { form, draft, busy, errors, message } = this.state;

    return (form &&
      <form>
        <Input
          label="ЧПУ"
          value={form.slug}
          change={this.handleChange('slug')}
          margin="normal"
          fullWidth
          disabled={true}
          errorText={ this.fieldError('slug') }
          helperText={ 'Генерируется автоматически' }
        />
        
        <Input
          label="Название"
          value={form.name}
          change={this.handleChange('name')}
          margin="normal"
          fullWidth
          disabled={busy}
          errorText={ this.fieldError('name') }
        />
        <Submit
          onSubmit={this.submit} 
          disabled={_.isEqual(form, item)} 
          loading={busy}
          message={message} 
        />
      </form>
    );
  }
}