import React from 'react';
import { DragDropContext } from "react-dnd";
import {history} from "../../core/history";
import HTML5Backend from 'react-dnd-html5-backend';
import { 
  List as MaterialList, 
  Button, 
  Typography, 
  Modal,
  withStyles,
  Theme
} from '@material-ui/core';

import { H1 } from '../../components/titles';
import Loader, { ShadowLoader } from '../../components/loader';
import { PaperPadded } from '../../components/paper';
import NestedList from '../../components/tree/nested-list';
import NestedListItemInterface from '../../components/tree/nested-list-item-interface';
import Form from './form';
import { apiGet, apiPatch, apiDelete } from '../../core/api';

const styles = (theme: Theme) => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  },
});

interface SportInterface {
  id: number;
  name: string;
  label: string;
  icon: File;
}

class Item implements NestedListItemInterface {
  name: string; 
  items: Item[] = [];
  open = false;
    private icon: File;
  constructor(
    public id: number
  ) {
  }

  data: SportInterface = {
    id: null,
    name: null,
    label: null,
    icon: null,
  };
  setData(data: SportInterface) {
    this.data = data;
    this.id = data.id;
    this.name = data.name;
    this.icon = data.icon;
  }
  setLabel(label: string) {
    this.data.label = label;
  }
  getLabel() {
    return this.data.label;
  }
}

class ListComponent extends React.Component<{
  classes: any,
}>
{
  state = {
    loading: true,
    busy: false,
    openEditModal: false,
    editItem: null,
    rootItem: new Item(null),
    i: 0,
  }

  constructor(props) {
    super(props);
    this.bootstrap();
  }

  bootstrap = async () => {
    const { rootItem } = this.state;
    const sports = (await apiGet('/sports'))['data'] as SportInterface[];
    console.log(sports);
    let itemsByLabel = {};
    for (let sport of sports) {
      var item = new Item(null);
      item.setData(sport);
      if (itemsByLabel.hasOwnProperty(sport.label)) {
        if (Array.isArray(itemsByLabel[sport.label])) {
          item.items = itemsByLabel[sport.label];
        } else {
          throw new Error();
        }
      }
      itemsByLabel[sport.label] = item;
      var arrayLabel = sport.label.split('.');
      if (arrayLabel.length === 1) {
        rootItem.items.push(item);
      } else {
        var parentLabel = arrayLabel.slice(0, arrayLabel.length - 1).join('.');
        if (itemsByLabel.hasOwnProperty(parentLabel)) {
          if (Array.isArray(itemsByLabel[parentLabel])) {
            itemsByLabel[parentLabel].push(item);
          } else {
            itemsByLabel[parentLabel].items.push(item);
            // throw new Error();
          }
        } else {
          itemsByLabel[parentLabel] = [item];
        }
      }
    }
    for (let [key, elem] of Object.entries(itemsByLabel)) {
      if (Array.isArray(elem)) {
        let dataForUpdate = [];
        for (let item of elem as Item[]) {
          rootItem.items.push(item);
          dataForUpdate = dataForUpdate.concat(this.runItems(item, null));
        }
        await apiPatch(`/sports/`, JSON.stringify({data: dataForUpdate}));
      }
    }

    console.log(rootItem);
    this.setState({
      loading: false,
    });
  }

  addItem = (item: Item) => {
    console.log('addItem', item);
    const newItem = new Item(null);
    newItem.setLabel(item.getLabel());
    item.items.push(newItem);
    item.open = true;
    this.editItem(newItem);
  }

  removeItem = async (item: Item, root: Item) => {
    this.setState({
      busy: true,
    });
    // let arrayLabel = item.getLabel().split('.');
    let parentLabel = root.getLabel();
    // if (arrayLabel.length > 1) {
    //   parentLabel = arrayLabel.slice(0, arrayLabel.length - 1).join('.');
    // }
    const dataForUpdate = this.runItems(item, parentLabel);
    const index = root.items.indexOf(item);
    if (index !== -1) {
      root.items.splice(index, 1);
    }
    root.items = root.items.concat(item.items);

    await apiPatch(`/sports/`, JSON.stringify({data: dataForUpdate}));
    await apiDelete(`/sports/${item.id}`);
    this.setState({
      busy: false,
    });
  }

  editItem = (item: Item) => {
     this.setState({
      openEditModal: true,
      editItem: item,
    });
  }

  goToEditPage = (item: Item) => {
    var win = window.open(`/sports/${item.id}/edit`, '_blank');
    win.focus();
  }

  onSubmited = (data: SportInterface) => {
    this.state.editItem.setData(data);
    this.closeEditModal();
  }

  closeEditModal = () => {
    this.setState({ 
      openEditModal: false,
      editItem: null,
    });
  };

  runItems = (item: Item, parentLabel: string) => {
    let data = [];
    item.setLabel((parentLabel ? parentLabel + '.' : '') + item.id);
    data.push({
      id: item.id,
      label: item.getLabel()
    });
    for (let child of item.items) {
      data = data.concat(this.runItems(child, item.getLabel()));
    }
    return data;
  }

  onMove = (sourceItem, oldIndex, targetItem, newIndex) => {
    if (sourceItem === targetItem && oldIndex === newIndex) {
      // nothing to do here
      return;
    }
    const [elem] = sourceItem.items.splice(oldIndex, 1) as Item[];
    if (sourceItem === targetItem && oldIndex < newIndex) {
      newIndex--;
    }
    if (targetItem.items.length === 0) {
      targetItem.open = true;
    }
    targetItem.items.splice(newIndex, 0, elem);
    const parentLabel = targetItem.getLabel();
    
    let dataForUpdate = this.runItems(elem, parentLabel);

    apiPatch(`/sports/`, JSON.stringify({data: dataForUpdate}));
    this.forceUpdate();
  }

  onToggle = (item:NestedListItemInterface) => {
    item.open = !item.open;
    this.forceUpdate();
  }

  render() {
    const { classes } = this.props;
    const { loading, busy, rootItem, openEditModal, editItem } = this.state;
    return (
      <React.Fragment>
        <H1>
          Виды спорта
        </H1>
        <Loader loading={loading}>
          <ShadowLoader loading={busy}>
            <PaperPadded>
              <MaterialList>
                <NestedList 
                  root={rootItem} 
                  addItem={this.addItem} 
                  removeItem={this.removeItem} 
                  editItem={this.goToEditPage}
                  onMove={this.onMove}
                  onToggle={this.onToggle}
                />
              </MaterialList>
              <Button 
                variant="outlined" 
                color="primary" 
                onClick={() => this.addItem(rootItem)}
              >
                Добавить корневой вид
              </Button>
            </PaperPadded>
          </ShadowLoader>
        </Loader>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={openEditModal}
          onClose={this.closeEditModal}
        >
          <div className={classes.paper}>
            {openEditModal && editItem && 
              <Form item={editItem.data} onSubmitted={this.onSubmited} />
            }
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export const List = DragDropContext(HTML5Backend)(withStyles(styles as any)(ListComponent));