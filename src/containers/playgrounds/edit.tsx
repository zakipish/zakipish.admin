import * as React from "react";

import { List, ListItem, ListItemText } from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import { Button} from "@material-ui/core";
import { IconButtonLink, ButtonLink } from "../../components/buttons";
import {apiGet, apiDelete} from "../../core/api";
import { history } from "../../core/history";
import { Playground } from "../../models/playground";
import Form from "./form";
import { BackButton, AddButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1, H2 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import OldSlugs from "../../components/old-slugs";
import UploadImage from "../../components/upload-image";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;
  
  state = {
    item: null,
    id: null,
    clubId: null,
    loading: true,
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }
  deletePlayground(id){
      const { clubId } = this.state;
      let result = apiDelete(`/playgrounds/${id}`);
      if (result){
          history.push(`/clubs/${clubId}/edit`);
      }
  }
  bootstrap = async () => {
    const { playground, club } = this.props.match.params;
    this.setState({
      id: playground,
      clubId: club,
    });
    try {
      const data = await apiGet(`/playgrounds/${playground}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push(`/clubs/${club}/edit`);
    }
  };

  onSubmit = (saved: Playground) => {
      const { clubId } = this.state;
    // history.push(`/clubs/${clubId}/edit`);
  };

  render() {
    const { id, clubId, loading, item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to={`/clubs/${clubId}/edit`} />
          Зал №{id} {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
            <PaperPadded>
              <Form item={item}  onSubmitted={this.onSubmit} />
              <OldSlugs slugs={item.slugs}></OldSlugs>
            </PaperPadded>
          )}
        </Loader>
        {item && <UploadImage url={`/playgrounds/${item.id}`} image={item.images} field="image[]" title="Фото" isMulti />}
          <Button variant="contained" color="secondary" size="large" style={{marginTop:"15px",marginRight :"15px"}} onClick={()=>this.deletePlayground(item.id)}>
              Удалить
              <DeleteIcon />
          </Button>
          <ButtonLink to={`/clubs/${clubId}/playgrounds/${id}/playground-schedules`} variant="contained" color="primary" size="large" style={{marginTop:"15px"}}>
            Изменить цены
        </ButtonLink>
      </React.Fragment>
    );
  }
}
