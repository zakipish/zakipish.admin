import * as React from "react";

import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper } from "@material-ui/core";
import { IconButtonLink, ButtonLink } from "../../components/buttons";
import { H2Flex } from "../../components/titles";
import Pagination from "../../components/pagination";
import { ExtendedTableCell, ThinTableCell } from "../../components/table";
import { Redirect } from 'react-router-dom';
export class ListForClub extends React.Component<{
  id: Number,
}> {
  get_edit_button(item){
    return <IconButtonLink to={`/clubs/${this.props.id}/playgrounds/${item.id}/edit`} aria-label="Edit">
      <EditIcon />
    </IconButtonLink>;
  }

  render() {
    const { id } = this.props;
    return (
      <React.Fragment>
        <H2Flex>
          <span>Залы</span>
          <ButtonLink
            variant="contained"
            size="large"
            color="primary" 
            to={`/clubs/${id}/playgrounds/create`}
          >
            Добавить зал
          </ButtonLink>
        </H2Flex>
        <Paper>
          <Pagination url={`/playgrounds?club=${id}`}>
            {items => (
              <Table aria-labelledby="tableTitle">
                <TableHead>
                  <TableRow style={{height:"34px"}}>
                    <ThinTableCell padding="dense">
                      Id
                    </ThinTableCell>
                    <TableCell>
                      Название
                    </TableCell>
                    <TableCell>
                      Дата удаления
                    </TableCell>
                    <TableCell>
                      ЧПУ
                    </TableCell>
                    <ExtendedTableCell thin={true}  padding="none">
                    </ExtendedTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map(item => (
                    <TableRow
                      style={{borderBottom:"1.5px solid #d7e8b8"}}
                      hover
                      key={item.id}
                    >
                      <ExtendedTableCell thin={true} disabled={item.deleted_at?true:false} numeric padding="dense">
                        {item.id}
                      </ExtendedTableCell>
                      <ExtendedTableCell disabled={item.deleted_at?true:false}>
                        {item.name}
                      </ExtendedTableCell>
                      <ExtendedTableCell disabled={item.deleted_at?true:false}>
                        {item.deleted_at}
                      </ExtendedTableCell>
                      <ExtendedTableCell disabled={item.deleted_at?true:false}>
                        {item.slug}
                      </ExtendedTableCell >
                      <ExtendedTableCell disabled={item.deleted_at?true:false}>
                        {item.deleted_at? "":this.get_edit_button(item)}
                      </ExtendedTableCell>

                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </Pagination>
        </Paper> 
      </React.Fragment>
    );
  }
};