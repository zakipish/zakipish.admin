import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import Form from "./form";
import { Playground } from "../../models/playground";
import { history } from "../../core/history";

export class Create extends React.Component<{
  match: any;
}> {

  state = {
    item: null,
    clubId: null,
  }

  constructor(props) {
    super(props);
    const { club } = this.props.match.params;
    this.state = {
      item: new Playground({'club_id': club}),
      clubId: club,
    };
  }

  onSubmit = (saved: Playground) => {
    const { clubId,item } = this.state;
    history.push(`/clubs/${clubId}/playgrounds/${saved.id}/edit`);
  };

  render() {
    const { item, clubId } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to={`/clubs/${clubId}/edit`} />
          Создание зала
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
