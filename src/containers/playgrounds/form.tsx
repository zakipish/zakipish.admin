import * as React from "react";
import * as _ from "lodash";

import Submit from "../../components/form/submit";
import AbstractForm from "../abstract-form";
import Select from "../../components/form/select-control";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Input from "../../components/form/input-control";
import {apiGet} from "../../core/api";
import SelectControl from "../../components/form/select-control";
import {FormControl, Grid, Button, IconButton} from "@material-ui/core";
import {H3} from "../../components/titles";
import DeleteOutline from '@material-ui/icons/DeleteOutline';

export default class Form extends AbstractForm<{}, {
  playgroundTypes?: any;
  services?: any;
  surfaces?: any;
  sports?: any;
  equipments?: any;
}> {
  apiUri = '/playgrounds/';

  types = [
    {
      value: '1',
      label: 'Открытый',
    },
    {
      value: '2',
      label: 'Закрытый',
    },
  ];
  // 1 минута
  // 5 минут
  // 10 минут
  // 15 минут
  // 30 минут
  // 1 час
  // 2 часа
  intervals = [
    {value: 1, label: '1 минута'},
    {value: 2, label: '2 минуты'},
    {value: 4, label: '4 минуты'},
    {value: 5, label: '5 минут'},
    {value: 6, label: '6 минут'},
    {value: 10, label: '10 минут'},
    {value: 12, label: '12 минут'},
    {value: 15, label: '15 минут'},
    {value: 20, label: '20 минут'},
    {value: 30, label: '30 минут'},
    {value: 60, label: '1 час'},
    {value: 120, label: '2 часа'},
  ];

  constructor(props) {
    super(props);
    this.getEquipments();
    this.getPlaygroundTypes();
    this.getServices();
    this.getSurfaces();
    this.getSports();
  }

  getEquipments = async () => {
    const data = await apiGet(`/equipments`, null, true);
    !this.unmounted && this.setState({
      equipments: data.data.map(el => ({value: el.id, label: el.name})),
    });
  };

  getPlaygroundTypes = async () => {
    const data = await apiGet(`/playground-types`, null, true);
    !this.unmounted && this.setState({
      playgroundTypes: data.data.map(el => ({value: el.id, label: el.name})),
    });
  };

  getSurfaces = async () => {
    const data = await apiGet(`/surfaces`, null, true);
    !this.unmounted && this.setState({
      surfaces: data.data.map(el => ({value: el.id, label: el.name})),
    });
  };

  getServices = async () => {
    const data = await apiGet(`/services`, null, true);

    !this.unmounted && this.setState({
      services: data.data.map(el => ({value: el.id, label: el.name})),
    });
  };

  //get all sports for choice in the selector
  getSports = async () => {
    const data = await apiGet(`/sports`, null, true);

    //sorting by label before delimiting
    data.data = data.data.sort((a, b) => {
      return a.label.localeCompare(b.label)
    });

    //delimiting
    let delimiterCount = null;
    let name = null;
    !this.unmounted && this.setState({
      sports: data.data.map(el => {
        //counting label-delimiters
        delimiterCount = el.label.split(".").length - 1;
        //insert delimiters
        name = el.name;
        for (let counter = 0; counter < delimiterCount; counter++) {
          name = '--' + name;
        }
        return ({
          value: el.id,
          label: name,
          parents: el.label,
        });
      }),
    });
  };

  addService = () => {
    const {form} = this.state
    form.services.push({
      id: null,
      price: '',
      unit: '',
    });
    this.setState({
      form,
    });
  };

  removeService = (index) => {
    const {form} = this.state
    form.services.splice(index, 1);
    this.setState({
      form,
    });
  };

  handleServiceValue = (index, key, value) => {
    console.log(index, key, value);
    const {form} = this.state
    form.services[index][key] = value;
    this.setState({
      form: {...form},
    });
  }

  addEquipment = () => {
    const {form} = this.state
    form.equipments.push({
      id: null,
      price: '',
      unit: '',
    });
    this.setState({
      form,
    });
  };

  removeEquipment = (index) => {
    const {form} = this.state
    form.equipments.splice(index, 1);
    this.setState({
      form,
    });
  };

  handleEquipmentValue = (index, key, value) => {
    console.log(index, key, value);
    const {form} = this.state
    form.equipments[index][key] = value;
    this.setState({
      form: {...form},
    });
  }

  //method for change checkbox on opposite value
  changeCheckbox = (index, key, value) => {
    const {form} = this.state;
    form.services[index][key] = value;
    this.setState({
      form: {...form},
    });
    // form.service[index][key] = value;
  }
  changeCheckboxEquipment = (index, key, value) => {
    const {form} = this.state;
    form.equipments[index][key] = value;
    this.setState({
      form: {...form},
    });
    // form.service[index][key] = value;
  }

  render() {
    const {item} = this.props;
    const {
      form, busy, message, playgroundTypes, sports,
      surfaces, services, equipments
    } = this.state;
    return (form &&
        <form>
            <Input
                label="ЧПУ"
                value={form.slug}
                change={this.handleChange('slug')}
                margin="normal"
                fullWidth
                disabled={true}
                error={this.fieldError('slug')}
                helperText={'Генерируется автоматически'}
            />

            <Input
                label="Название"
                value={form.name}
                change={this.handleChange('name')}
                margin="normal"
                fullWidth
                disabled={busy}
                error={this.fieldError('тфьу')}
            />

            <Input
                style={{marginTop: "0", marginBottom: "16px"}}
                label="Текстовое описание"
                value={form.description}
                change={this.handleChange('description')}
                margin="normal"
                fullWidth
                disabled={busy}
                multiline
                rows={5}
                errorText={this.fieldError('description')}
            />

            <Select
                suggestions={this.types}
                value={form.type}
                label="Тип зала"
                margin="normal"
                fullWidth
                onChange={this.handleChange('type')}
                errorText={this.fieldError('type')}
                placeholder="Тип зала"
            />

            <Select
                suggestions={this.intervals}
                value={form.min_interval}
                label="Интервал бронирования"
                margin="normal"
                fullWidth
                onChange={this.handleChange('min_interval')}
                errorText={this.fieldError('min_interval')}
                placeholder="Интервал бронирования"
            />

          {surfaces && <SelectControl
              suggestions={surfaces}
              value={form.surfaces}
              label="Поверхность"
              margin="normal"
              fullWidth
              isMulti
              onChange={this.handleChange('surfaces')}
              placeholder="Выбор поверхностей"
          />}

          {sports && <SelectControl
              suggestions={sports}
              value={form.sports}
              label="Виды спорта"
              margin="normal"
              fullWidth
              isMulti
              onChange={this.handleSportsChange}
          />}

          {form.services && <FormControl style={{marginTop: "8px"}} margin="normal" fullWidth>
              <H3>Услуги</H3>
            {form.services.map((service, i) => (

              <Grid item xs={12} container alignItems="center" spacing={16} key={i}>

                <Grid item xs={12} sm={2}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={(service.payable === true) ? service.payable = true : service.payable}
                        onChange={(checked) => this.changeCheckbox(i, 'payable', !service.payable)}
                      />
                    }
                    labelPlacement='end'
                    label='Платно'
                  />
                </Grid>


                <Grid item xs={12} sm>
                  {services && <SelectControl
                      suggestions={services}
                      value={service.id}
                      label="Услуги"
                      margin="normal"
                      fullWidth
                      onChange={(value) => this.handleServiceValue(i, 'id', value)}
                      placeholder="Выбор названия услуги"
                  />}
                </Grid>
                <Grid item xs={12} sm>
                  <Input
                    label="Цена"
                    value={service.price}
                    change={(value) => this.handleServiceValue(i, 'price', value)}
                    fullWidth
                    margin="none"
                    disabled={busy}
                  />
                </Grid>
                <Grid item xs={12} sm>
                  <div style={{
                    display: 'flex',

                  }}>
                    <Input
                      label="Ед."
                      value={service.unit}
                      change={(value) => this.handleServiceValue(i, 'unit', value)}
                      fullWidth
                      margin="none"
                      disabled={busy}
                    />
                    <IconButton onClick={() => this.removeService(i)}>
                      <DeleteOutline/>
                    </IconButton>
                  </div>
                </Grid>
              </Grid>
            ))}
              <FormControl margin="normal">
                  <Button
                      variant="outlined"
                      color="primary"
                      onClick={this.addService}
                  >
                      Добавить услугу
                  </Button>
              </FormControl>
          </FormControl>}

          {form.equipments && <FormControl margin="normal" fullWidth>
              <H3>Инвентарь</H3>
            {form.equipments.map((equipment, i) => (
              <Grid item xs={12} container alignItems="center" spacing={16} key={i}>
                <Grid item xs={12} sm={2}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={(typeof equipment.payable === 'undefined') ? equipment.payable = true : equipment.payable}
                        onChange={(checked) => this.changeCheckboxEquipment(i, 'payable', !equipment.payable)}
                      />
                    }
                    labelPlacement='end'
                    label='Платно'
                  />
                </Grid>
                <Grid item xs={12} sm={5}>
                  {equipments && <SelectControl
                      suggestions={equipments}
                      value={equipment.id}
                      label="Инвентарь"
                      margin="normal"
                      fullWidth
                      onChange={(value) => this.handleEquipmentValue(i, 'id', value)}
                      placeholder="Выбор названия инвентаря"
                  />}
                </Grid>
                <Grid item xs={12} sm={2}>
                  <Input
                    label="Цена"
                    value={equipment.price}
                    change={(value) => this.handleEquipmentValue(i, 'price', value)}
                    fullWidth
                    margin="none"
                    disabled={busy}
                  />
                </Grid>
                <Grid item xs={12} sm={3}>
                  <div style={{
                    display: 'flex',

                  }}>
                    <Input
                      label="Ед."
                      value={equipment.unit}
                      change={(value) => this.handleEquipmentValue(i, 'unit', value)}
                      fullWidth
                      margin="none"
                      disabled={busy}
                    />
                    <IconButton onClick={() => this.removeEquipment(i)}>
                      <DeleteOutline/>
                    </IconButton>
                  </div>
                </Grid>
              </Grid>
            ))}
              <FormControl margin="normal">
                  <Button
                      variant="outlined"
                      color="primary"
                      onClick={this.addEquipment}
                  >
                      Добавить инвентарь
                  </Button>
              </FormControl>
          </FormControl>}

            <Submit
                float
                onSubmit={this.submit}
                disabled={_.isEqual(form, item)}
                loading={busy}
                message={message}
            />
        </form>
    );
  }
}
