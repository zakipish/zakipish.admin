import * as React from "react";

import {Link} from "react-router-dom";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'

import {H1} from "../../components/titles";
import {AddButton} from "../../components/buttons";

export class Roles extends React.Component<{}, {}> {
  types = [
    {value:0, label:'Пользователи'},
    {value:1, label:'Администраторы'},
    {value:2, label:'Модераторы'},
  ];
  render() {


    return (
      <React.Fragment>
        <H1>Роли пользователей</H1>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Grid container justify="flex-start" spacing={16}>

              {this.types.map((type)=>{
                //link for button
                const UserLink = props => <Link to={`/users-roles/${type.value}`} {...props} />;
                return (
                <Grid key={type.value} item>
                  <Card>
                    <CardContent>
                      <Typography gutterBottom variant="title" component="h2">
                        {type.label}
                      </Typography>
                      </CardContent>
                    <CardActions>
                        <Button size="small" color='primary' component={UserLink}>Перейти</Button>
                    </CardActions>
                  </Card>
                </Grid>
              )
              })}
            </Grid>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
};
