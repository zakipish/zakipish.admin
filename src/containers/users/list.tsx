import * as React from "react";
import {Link} from "react-router-dom";
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Paper} from "@material-ui/core";

import {IconButtonLink, AddButton, BackButton} from "../../components/buttons";
import {H1} from "../../components/titles";
import Pagination from "../../components/pagination";
import {ThinTableCell} from "../../components/table";

export class List extends React.Component<{ match?: any }> {
  apiUri = '/users/';
  types = [
    {value: '0', label: 'Пользователи'},
    {value: '1', label: 'Администраторы'},
    {value: '2', label: 'Модераторы'},
  ];

  render() {
    const roleId = this.props.match.params.role;
    return (
      <React.Fragment>
        <H1>
          <BackButton to={`/users-roles/`}/>
          {this.types[roleId].label}
        </H1>
        <Paper>
          <Pagination url={"/users?role=" + roleId}>
            {items => (
              <Table aria-labelledby="tableTitle">
                <TableHead>
                  <TableRow>
                    <ThinTableCell>
                    </ThinTableCell>
                    <TableCell>Имя</TableCell>
                    <TableCell>Email</TableCell>
                      {roleId==2?<TableCell>Модерируеммые клубы</TableCell>:null}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map(item => (
                      <TableRow
                          hover
                          key={item.id}
                      >
                          <ThinTableCell>
                              <IconButtonLink to={`/users/${item.id}/edit`} aria-label="Edit">
                                  <EditIcon/>
                              </IconButtonLink>
                          </ThinTableCell>
                          <TableCell>
                              <Link to={`/users/${item.id}/edit`} aria-label="Edit">{item.name}</Link>
                          </TableCell>
                          <TableCell>{item.email}</TableCell>
                          {roleId == 2 ?
                              <TableCell>
                                  {item.moderated_clubs.map((club, i) => <span key={i}>
                                      <Link
                                      to={`/clubs/${club.id}/edit`}>{club.name}</Link>
                                      {i < item.moderated_clubs.length - 1 ? ', ' : null}
                                      </span>)}
                              </TableCell>
                              : null}

                      </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </Pagination>
        </Paper>
        <AddButton to={`/users/create/${roleId}`}/>
      </React.Fragment>
    );
  }
};
