import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import Form from "./form";
import { User } from "../../models/user";
import { history } from "../../core/history";

export class Create extends React.Component<{match:any},{item: User}> {

  state = {
    item: new User(),
  };

  onSubmit = (saved: User) => {
    history.push(`/users/${saved.id}/edit/`);
  };

  render() {
    const { item } = this.state;
    const { role } = this.props.match.params;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/users-roles" />
          Создание пользователя
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} role={role} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
