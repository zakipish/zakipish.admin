import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { User } from "../../models/user";
import { history } from "../../core/history";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;

  state = {
    item: null,
    id: null,
    loading: true,
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }

  bootstrap = async () => {
    const { user } = this.props.match.params;
    this.setState({
      id: user,
    });
    try {
      const data = await apiGet(`/users/${user}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push('/users');
    }
  };

  onSubmit = (saved: User) => {
    this.setState({
      item: new User(saved),
    });
  };

  render() {
    const { id, loading, item } = this.state;
    console.log('item', item);
    return (
      <React.Fragment>
        <H1>
          <BackButton to={(item) ? `/users-roles/${item.role}` : `/users-roles/`} />
          Пользователь №{id} {item ? `"${item.email}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
            <PaperPadded>
              <Form item={item} onSubmitted={this.onSubmit} />
            </PaperPadded>
          )}
        </Loader>
      </React.Fragment>
    );
  }
}
