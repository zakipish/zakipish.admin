import * as React from "react";
import * as _ from "lodash";

import Submit from "../../components/form/submit";
import AbstractForm from "../abstract-form";
import Input from "../../components/form/input-control";
import {apiGet} from "../../core/api";
import SelectControl from "../../components/form/select-control";
import DateIntervalControl from "../../components/form/DateIntervalControl";
import DebounceSelect from "../../components/form/select-debounce";
import Grid from "@material-ui/core/Grid";
import {Button, FormControl, IconButton} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import {H3} from "../../components/titles";

export default class Form extends AbstractForm<{role?:number}, {
  clubs?: any;
}> {
  apiUri = '/users/';
  types = [
    {value: '0', label: 'Пользователи'},
    {value: '1', label: 'Администраторы'},
    {value: '2', label: 'Модераторы'},
  ];


  constructor(props) {
    super(props);
    this.state.clubs = [];
  }

  componentDidMount = () => {
    //устанваливаем роль нулевой, если она остутствует (например для создания) или берем из параметов
    const {form} = this.state;
    const {role} = this.props;
    if (!form.role) {
      form.role = role? role:'0';
      this.setState({
        form,
      });
    }
  };


  //handle select in selector
  handleDebounceClubsChange = (index: number, value: any) => {
    const {form} = this.state;
    form.moderated_clubs[index].id = value.value;
    form.moderated_clubs[index].name = value.label;
    this.setState({
      form: {...form},
    });

  };

  //handle debounce search onInputValue event
  handleDebounceSelectClubsSearch = async (value, callback) => {
    //if value exist then request
    let clubs = [];
    if (value.length >= 1) {
      const response = await apiGet(`/clubs`, {name: value, id: value}, true);
      clubs = response.data.map(el => ({
        label: (el.name),
        value: el.id
      }));
      callback(clubs);
    }
    this.setState({
      clubs,
    });
  };

  //delete club
  deleteClub = (club) => {
    let newClubs = this.state.form.moderated_clubs.filter(item => item.id !== club.id);
    let newForm = this.state.form;
    newForm.moderated_clubs = newClubs;
    this.setState({
      form: newForm,
    });
  };

  addClub = () => {
    const {form} = this.state;
    form.moderated_clubs.push({
      id: null,
      name: null,
    });
    this.setState({
      form,
    });
    console.log('form', form);
  };

  moderateClubsElement = () => {
    const {form} = this.state;
    if (form.role === 2){
      return ((form.moderated_clubs) &&
          <FormControl margin="normal" fullWidth>
              <H3>Подконтрольные клубы</H3>
            {
              (form.moderated_clubs) && form.moderated_clubs.map((club, i) => (
                <Grid container spacing={8} key={i}>
                  <Grid item xs={11} >
                    <DebounceSelect
                      label="Имя клуба"
                      value={{value: club.id, label: club.name}}
                      placeholder="Начните вводить имя или email"
                      onChange={(value) => this.handleDebounceClubsChange(i, value)}
                      loadOptions={this.handleDebounceSelectClubsSearch}
                      latitude={1200}
                      margin="normal"
                      emptyMessage='Для поиска начните вводить имя или ID клуба'
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={1} style={{marginTop: 25}}>
                    <IconButton style={{marginRight: "5px"}} aria-label="Delete"
                                onClick={() => this.deleteClub(club)}>
                      <DeleteIcon/>
                    </IconButton>
                  </Grid>

                </Grid>
              ))}
              <Grid xs={12} item container justify="flex-end" alignItems="center">
                  <Button
                      variant="contained"
                      color="primary"
                      size="large"
                      onClick={this.addClub}
                      style={{float: 'right'}}
                  >
                      Добавить клуб
                  </Button>
              </Grid>
          </FormControl>

      );
    }
  }
  render() {
    const {item} = this.props;
    const {form, busy, message,} = this.state;
    console.log('form', this.state);
    return (form &&
        <form>
            <Grid>
                <Input
                    label="Email"
                    value={form.email}
                    change={this.handleChange('email')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    errorText={this.fieldError('email')}
                />
            </Grid>
            <Grid>
                <Input
                    label="Password"
                    value={form.password}
                    change={this.handleChange('password')}
                    type="password"
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    errorText={this.fieldError('password')}
                />
            </Grid>
            <Grid>
                <Input
                    label="Имя"
                    value={form.name}
                    change={this.handleChange('name')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    errorText={this.fieldError('name')}
                />
            </Grid>

            <SelectControl
                label="Роль пользователя"
                fullWidth
                onChange={this.handleChange('role')}
                value={(form.role) ? form.role.toString() : '0'}
                suggestions={this.types}
                errorText={this.fieldError('role')}

            />

          {this.moderateClubsElement()}

            <Submit
                float
                onSubmit={this.submit}
                disabled={_.isEqual(form, item)}
                loading={busy}
                message={message}
            />
        </form>
    );
  }
}