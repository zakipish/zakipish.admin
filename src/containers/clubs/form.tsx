import * as React from "react";
import * as _ from "lodash";

import {Grid, withStyles, Button, FormControl, IconButton} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';

import {apiGet, apiPost} from "../../core/api";
import Submit from "../../components/form/submit";
import Select from "../../components/form/select-control";
import Input from "../../components/form/input-control";
import InputEditor from "../../components/form/input-editor";
import Expansion from "../../components/form/expansion";
import AbstractForm from "../abstract-form";
import {H3, H2, H3Contact, H3Geo} from "../../components/titles";
import SelectControl from "../../components/form/select-control";
import DebounceSelect from "../../components/form/select-debounce";
import {PaperPadded} from "../../components/paper";

const styles = theme => ({
  noMarginTop: {
    marginTop: 0,
  },
  expansion: {
    marginTop: theme.spacing.unit * 2,
  },
  expansionExpanded: {
    overflow: 'visible',
  },
});

class Form extends AbstractForm<{
  classes: any
}, {
  clubChains: any;
  contactTypes: any;
  cities: any;
  departments: any;
  infrastructures: any;
  sports?: any;
  clubs?: any;
  users?: any;
}> {
  apiUri = '/clubs/';

  ranks = [
    {
      value: '0',
      label: '0',
    },
    {
      value: '1',
      label: '1',
    },
    {
      value: '2',
      label: '2',
    },
    {
      value: '3',
      label: '3',
    },
    {
      value: '4',
      label: '4',
    },
  ];

  types = [
    {
      value: null,
      label: ''
    },
    {
      value: '1',
      label: 'Зал'
    },
    {
      value: '2',
      label: 'Секция'
    },
    {
      value: '3',
      label: 'Секция/зал'
    },
  ];

  booking_status = [
    {value: 1, label: 'Недоступен'},
    {value: 2, label: 'Доступен'},
  ];

  constructor(props) {
    super(props);
    this.state.form = this.cloneModel(props.item);
    this.state.clubs = [];
    this.getClubChains();
    this.getContactTypes();
    this.getCities();
    this.getInfrastructures();
    this.getSports();
    this.state.form.city_id?this.getDepartments(this.state.form.city_id):null;
  }
    handleChangeWithDepartment = (name: string) => (value: any) => {
        const {form} = this.state;
        form[name] = value;
        this.setState({
            form,
        },()=>{
            this.getDepartments(value);
        });
        // console.log(form)
    };
    getDepartments = async (id) => {
      const chainsData = await apiGet(`/departments_all/${id}`, null);
      !this.unmounted && this.setState({
          departments: chainsData.data.map(el => ({value: el.id, label: el.name})),
      });
  };
  getClubChains = async () => {
    const chainsData = await apiGet(`/club-chains`, null, true);
    !this.unmounted && this.setState({
      clubChains: chainsData.data.map(el => ({value: el.id, label: el.name})),
    });
  };
  getContactTypes = async () => {
    const typesData = await apiGet(`/contact-types`, null, true);
    !this.unmounted && this.setState({
      contactTypes: typesData.data.map(el => ({value: el.id, label: el.name, id: el.id})),
    });
  };
  getCities = async () => {
    const typesData = await apiGet(`/cities`, null, true);
    !this.unmounted && this.setState({
      cities: typesData.data.map(el => ({value: el.id, label: el.name, id: el.id})),
    },);
  };
  getInfrastructures = async () => {
    const typesData = await apiGet(`/infrastructures`, null, true);
    !this.unmounted && this.setState({
      infrastructures: typesData.data.map(el => ({value: el.id, label: el.name, id: el.id})),
    });
  };

  addContact = () => {
    const {form} = this.state;
    form.contacts.push({
      value: '',
      contact_type_id: null,
    });
    this.setState({
      form,
    });
  };
  deleteContact = (index) => {
    this.state.form.contacts.splice(index, 1);
    let newContacts = this.state.form.contacts;
    let newForm = this.state.form;
    newForm.contacts = newContacts;
    this.setState({
      form: newForm,
    });

  };
  handleContactValue = (index, value) => {
    const {form} = this.state;
    form.contacts[index].value = value;
    this.setState({
      form: {...form},
    });
  };

  handleContactType = (index, value) => {
    const {form} = this.state;
    form.contacts[index].contact_type_id = value;
    this.setState({
      form: {...form},
    });
  };

  addBookingType = () => {
    const {form} = this.state;
    form.booking_types.push({
      type: '',
      status: 0,
      id: null,
      comment: '',
    });
    this.setState({
      form,
    });
  };
  deleteBookingType = (type) => {
    let newBookingType = this.state.form.booking_types.filter(item => item.id !== type.id);
    let newForm = this.state.form;http://admin.zakipish.local:8080/services
    newForm.booking_types = newBookingType;
    this.setState({
      form: newForm,
    });

  };
  handleBookingStatus = (index, value) => {

    const {form} = this.state;
    form.booking_types[index].status = value;
    this.setState({
      form: {...form},
    });
    console.log(this.state.form)
  };

  handleBookingTypeComment = (index, value) => {
    const {form} = this.state;
    form.booking_types[index].comment = value;
    this.setState({
      form: {...form},
    });
  };

  handleBookingType = (index, value) => {
    const {form} = this.state;
    form.booking_types[index].type = value;
    this.setState({
      form: {...form},
    });
  };

  //get all sports for choice in the selector
  getSports = async () => {
    const data = await apiGet(`/sports`, null, true);

    //sorting by label before delimiting
    data.data = data.data.sort((a, b) => {
      return a.label.localeCompare(b.label)
    });

    //delimiting
    let delimiterCount = null;
    let name = null;
    !this.unmounted && this.setState({
      sports: data.data.map(el => {
        //counting label-delimiters
        delimiterCount = el.label.split(".").length - 1;
        //insert delimiters
        name = el.name;
        for (let counter = 0; counter < delimiterCount; counter++) {
          name = '--' + name;
        }
        return ({
          value: el.id,
          label: name,
          parents: el.label,
        });
      }),
    });
  };

  //handle debounce search onInputValue event
  handleDebounceSelectSearch = async (value, callback) => {
    //if value exist then request
    console.log('DEBOUNCE CHANGE', value);
    let clubs = [];
    if (value.length > 0) {
      const parsedValue = parseInt(value);
      const response = await apiGet(`/clubs`, {name: value, id: parsedValue ? parsedValue : ""}, true);
      clubs = response.data.map(el => ({
        label: ('#' + el.id + ' ' + el.name),
        value: el.id
      }));
      callback(clubs);
    }
    this.setState({
      clubs: clubs,
    });
  }

  handleDebounceClubChange = (value: any) => {
    const {form} = this.state;
    console.log(value.value);
    form.parent_club_id = value.value;
    form.parent_club = {id: value.value, name: value.label};
    this.setState({
      form: {...form},
    });

  };

  //handle select in selector
  handleDebounceUsersChange = (index: number, value: any) => {
    const {form} = this.state;
    form.moderators[index].id = value.value;
    form.moderators[index].name = value.label;
    this.setState({
      form: {...form},
    });

  };

  //handle debounce search onInputValue event
  handleDebounceSelectUsersSearch = async (value, callback) => {
    //if value exist then request
    let users = [];
    if (value.length >= 3) {
      const response = await apiGet(`/users`, {search: value, role: 2}, true);
      users = response.data.map(el => ({
        label: (el.name + ' | ' + el.email),
        value: el.id
      }));
      callback(users);
    }
    this.setState({
      users,
    });
  };

  //delete club
  deleteUser = (user) => {
    let newClubs = this.state.form.moderators.filter(item => item.id !== user.id);
    let newForm = this.state.form;
    newForm.moderators = newClubs;
    this.setState({
      form: newForm,
    });
  };

  addUser = () => {
    const {form} = this.state;
    form.moderators.push({
      id: null,
      name: null,
    });
    this.setState({
      form,
    });
    console.log('form', form);
  };

  moderatorsComponent = () => {
    const {form} = this.state;
    return ((form.moderators) &&
        <FormControl margin="normal" fullWidth>
            <H2>Модераторы</H2>
          {
            (form.moderators) && form.moderators.map((user, i) => (
              <Grid container spacing={8} key={i}>
                <Grid item xs={11}>
                  <DebounceSelect
                    label="Имя клуба"
                    value={{value: user.id, label: (user.name) ? user.name + ' | ' + user.email : null}}
                    placeholder="Начните вводить имя или email"
                    onChange={(value) => this.handleDebounceUsersChange(i, value)}
                    loadOptions={this.handleDebounceSelectUsersSearch}
                    latitude={1200}
                    margin="normal"
                    emptyMessage='Для поиска начните вводить имя или емейл'
                    fullWidth
                  />
                </Grid>
                <Grid item xs={1} style={{marginTop: 25}}>
                  <IconButton style={{marginRight: "5px"}} aria-label="Delete"
                              onClick={() => this.deleteUser(user)}>
                    <DeleteIcon/>
                  </IconButton>
                </Grid>

              </Grid>
            ))}
            <Grid xs={12} item container justify="flex-end" alignItems="center">
                <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    onClick={this.addUser}
                    style={{float: 'right'}}
                >
                    Добавить модератора
                </Button>
            </Grid>
        </FormControl>

    );
  }


  render() {
    const {item} = this.props;
    const {form, busy, message, clubChains, contactTypes, cities, infrastructures, sports, departments} = this.state;
    // console.log('forms', form)
    // @ts-ignore
    return (form &&
        <form>
            <PaperPadded>
                <DebounceSelect
                    label="Родитель"
                    value={(form.parent_club) && {value: form.parent_club.id, label: form.parent_club.name}}
                    onChange={(value) => this.handleDebounceClubChange(value)}
                    loadOptions={this.handleDebounceSelectSearch}
                    latitude={1200}
                    margin="normal"
                    fullWidth
                />
                <Input
                    label="ЧПУ"
                    value={form.slug}
                    change={this.handleChange('slug')}
                    margin="normal"
                    fullWidth
                    disabled={true}
                    errorText={this.fieldError('slug')}
                    helperText={'Генерируется автоматически'}
                />

                <Input
                    label="Название"
                    value={form.name}
                    change={this.handleChange('name')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    errorText={this.fieldError('name')}
                />

                <InputEditor
                    label="Текстовое описание"
                    value={form.description}
                    change={this.handleChange('description')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    multiline
                    rows={5}
                    errorText={this.fieldError('description')}
                />

                <Select
                    suggestions={this.types}
                    value={form.type}
                    label="Тип"
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange('type')}
                    errorText={this.fieldError('type')}
                    placeholder="Выбор типа"
                />

                <Input
                  label="Цена кипиша"
                  value={form.event_price}
                  change={this.handleChange('event_price')}
                  fullWidth
                  margin="none"
                  disabled={busy}
                />

              {clubChains && <Select
                  suggestions={clubChains}
                  value={form.club_chain_id}
                  label="Сеть клубов"
                  margin="normal"
                  fullWidth
                  onChange={this.handleChange('club_chain_id')}
                  placeholder="Выбор сети клуба"
              />}

                <Select
                    suggestions={this.ranks}
                    value={form.rank}
                    label="Ранг"
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange('rank')}
                    errorText={this.fieldError('rank')}
                    placeholder="Выбор ранга"
                />

              {form.contacts && <FormControl margin="normal" fullWidth>
                  <H3Contact>Контакты</H3Contact>
                {form.contacts.map((contact, i) => (
                  <Grid item xs={12} container alignItems="center" spacing={16} key={i}>
                    <Grid item xs={12} sm>
                      {contactTypes && <Select
                          suggestions={contactTypes}
                          value={contact.contact_type_id}
                          margin="normal"
                          fullWidth
                          style={{marginTop: "34px"}}
                          onChange={(value) => this.handleContactType(i, value)}
                          placeholder="Выбор типа контакта"
                      />}
                    </Grid>
                    <Grid item xs={12} sm>
                      <Input
                        label="Значение"
                        value={contact.value}
                        change={(value) => this.handleContactValue(i, value)}
                        fullWidth
                        margin="none"
                        disabled={busy}
                      />
                    </Grid>
                    <Grid item xs={12} sm={1}>
                      <IconButton style={{marginRight: "5px"}} aria-label="Delete"
                                  onClick={() => this.deleteContact(i)}>
                        <DeleteIcon/>
                      </IconButton>
                    </Grid>
                  </Grid>
                ))}

                  <Grid xs={12} item container justify="flex-end" alignItems="center">
                      <Button
                          variant="contained"
                          color="primary"
                          size="large"
                          onClick={this.addContact}
                          style={{float: 'right'}}
                      >
                          Добавить контакт
                      </Button>
                  </Grid>
              </FormControl>}

                <Input
                    label="Адрес"
                    value={form.address}
                    change={this.handleChange('address')}
                    margin="normal"
                    fullWidth
                    disabled={busy}
                    errorText={this.fieldError('address')}
                />

                <FormControl style={{marginBottom: "-16px"}} margin="normal" fullWidth>
                  {/* <Expansion title="Адрес"> */}
                    <H3Geo>Геолокация</H3Geo>
                    <Grid container spacing={16}>
                        <Grid item xs={12} sm>
                            <Input
                                label="Страна"
                                value={form.country}
                                change={this.handleChange('country')}
                                fullWidth
                                margin="none"
                                disabled={busy}
                                errorText={this.fieldError('country')}
                            />
                        </Grid>
                        <Grid item xs={12} sm>
                            <Input
                                label="Регион"
                                value={form.region}
                                change={this.handleChange('region')}
                                fullWidth
                                margin="none"
                                disabled={busy}
                                errorText={this.fieldError('region')}
                            />
                        </Grid>
                        <Grid item xs={12} sm>
                          {cities && <Select
                              suggestions={cities}
                              value={form.city_id}
                              label="Город"
                              margin="none"
                              fullWidth
                              style={{marginTop: "-7px"}}
                              onChange={this.handleChangeWithDepartment('city_id')}
                          />}
                        </Grid>
                        <Grid item xs={12} sm>
                            {departments && <Select
                                suggestions={departments}
                                value={form.department_id}
                                label="Район Города"
                                margin="none"
                                fullWidth
                                style={{marginTop: "-7px"}}
                                onChange={this.handleChange('department_id')}
                            />}
                        </Grid>
                    </Grid>
                  {/* </Expansion> */}
                </FormControl>
              {/* <Expansion title="Extra"> */}
              {/* <Grid item xs> */}
                <Input
                    label="Источник"
                    value={form.source}
                    change={this.handleChange('source')}
                    fullWidth
                    placeholder="Гис, crm..."
                    margin="normal"
                    disabled={busy}
                    errorText={this.fieldError('source')}
                />
                <Input
                    label="Ссылка на источник карточки клуба"
                    value={form.source_link}
                    change={this.handleChange('source_link')}
                    fullWidth
                    margin="normal"
                    disabled={busy}
                    errorText={this.fieldError('source_link')}
                />
              {/* </Grid> */}
              {/* </Expansion> */}
                <Input
                    label="Координаты"
                    value={form.coordinates}
                    change={this.handleChange('coordinates')}
                    fullWidth
                    margin="normal"
                    disabled={busy}
                    errorText={this.fieldError('coordinates')}
                />

              {sports &&
              <FormControl margin="normal" fullWidth>

                  <H3>Виды спорта</H3>

                  <SelectControl
                      suggestions={sports}
                      value={form.sports}
                      margin="normal"
                      fullWidth
                      isMulti
                      onChange={this.handleSportsChange}
                  /></FormControl>
              }


              {infrastructures && <FormControl margin="normal" fullWidth>
                  <H3>Инфраструктура</H3>
                  <SelectControl
                      suggestions={infrastructures}
                      value={form.infrastructures}
                      margin="normal"
                      fullWidth
                      isMulti
                      onChange={this.handleChange('infrastructures')}
                  /></FormControl>}

              {form.booking_types && <FormControl margin="normal" fullWidth>
                  <H3>Тип бронирования</H3>
                {form.booking_types.map((type, i) => (
                  <Grid item xs={12} container alignItems="center" spacing={16} key={i}>
                    <Grid item xs={12} sm>
                      {this.booking_status && <Select
                          suggestions={this.booking_status}
                          value={type.status}
                          margin="normal"
                          fullWidth
                          style={{marginTop: "34px"}}
                          onChange={(value) => this.handleBookingStatus(i, value)}
                          placeholder="Выбор статуса"
                      />}
                    </Grid>

                    <Grid item xs={12} sm>
                      <Input
                        label="Тип"
                        value={type.type}
                        change={(value) => this.handleBookingType(i, value)}
                        fullWidth
                        margin="none"
                        disabled={busy}
                      />
                    </Grid>

                    <Grid item xs={12} sm>
                      <Input
                        label="Комментарий"
                        value={type.comment}
                        change={(value) => this.handleBookingTypeComment(i, value)}
                        fullWidth
                        margin="none"
                        disabled={busy}
                      />
                    </Grid>
                    <Grid item xs={12} sm={1}>
                      <IconButton style={{marginRight: "5px"}} aria-label="Delete"
                                  onClick={() => this.deleteBookingType(type)}>
                        <DeleteIcon/>
                      </IconButton>
                    </Grid>
                  </Grid>
                ))}
                  <Grid xs={12} item container justify="flex-end" alignItems="center">
                      <Button
                          variant="contained"
                          color="primary"
                          size="large"
                          onClick={this.addBookingType}
                          style={{float: 'right'}}
                      >
                          Добавить тип бронирования
                      </Button>
                  </Grid>

              </FormControl>
              }
            </PaperPadded>

            <PaperPadded style={{marginTop: 50}}>
              {this.moderatorsComponent()}
            </PaperPadded>

            <Submit
                float
                onSubmit={this.submit}
                disabled={_.isEqual(form, item)}
                loading={busy}
                message={message}
            />

        </form>
    );
  }
}

export default withStyles(styles)(Form);
