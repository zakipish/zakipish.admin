import * as React from "react";
import {Link} from "react-router-dom";
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {FormHelperText, Paper} from "@material-ui/core";

import {IconButtonLink, AddButton} from "../../components/buttons";
import {H1} from "../../components/titles";
import Pagination from "../../components/pagination";
import {ThinTableCell, ExtendedTableCell} from "../../components/table";
import {apiGet} from "../../core/api";
import Select from "../../components/form/select-control";
import Input from "../../components/form/input-debounce";
import {history} from "../../core/history";
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import DeleteIcon from '@material-ui/icons/Delete';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';

export class List extends React.Component<{}, {
  sortedField: string,
  sortedAsk: boolean,
  club_chain_id: any;
  city_id: any;
  name: any;
  debName: any;
  source: any;
  contactTypes: any;
  rank: string;
  infrastructures: any;
  sport_id: any;
  cities: any;
  sports: any;
  sources: any;
  clubChains: any;
}> {
  apiUri = '/clubs/';
  child;
  ranks = [
    {
      value: '0',
      label: '0',
    },
    {
      value: '1',
      label: '1',
    },
    {
      value: '2',
      label: '2',
    },
    {
      value: '3',
      label: '3',
    },
    {
      value: '4',
      label: '4',
    },
  ];

  query_params = new URLSearchParams(window.location.search);
  state = {
    sortedField: this.query_params.has('sorted_field') ? this.query_params.get('sorted_field') : "",
    sortedAsk: this.query_params.has('sorted_order') && this.query_params.get('sorted_order') === "desc" ? false : true,
    club_chain_id: "",
    city_id: "",
    debName: this.query_params.has('name') ? this.query_params.get('name') : "",
    source: this.query_params.has('source') ? this.query_params.get('source') : "",
    name: this.query_params.has('name') ? this.query_params.get('name') : "",
    contactTypes: null,
    rank: this.query_params.has('rank') ? this.query_params.get('rank') : "",
    infrastructures: null,
    sport_id: "",
    cities: [{value: "", label: ""}],
    sports: [{value: "", label: ""}],
    sources: [{value: "", label: ""}],
    clubChains: [{value: "", label: ""}],
  };

  getClubSports(item) {
    let sports = [];
    for (let sport of item.sportList) {
      sports.push(sport);
    }
    return sports.join(', ');
  }

  createQueryObject() {
    //Функция упаковывает все выбранные фильтры в один объект
    return {
      city: this.state.city_id || 0,
      club: this.state.club_chain_id || 0,
      name: this.state.name || 0,
      rank: this.state.rank || 0,
      sport: this.state.sport_id || 0,
    }
  }
  ;

  componentDidMount() {
    this.getFilters();
    // this.getSports();
  }

  constructor(props) {
    super(props);

  }

  getFilters = async () => {
    const filtersData = await apiGet(`/club-filters`, null, true);
    console.log("Выводим то что в фильтрах", filtersData);
    filtersData && this.setState(
      {
        cities: filtersData.cities.map(el => ({value: el.id, label: el.name})),
        clubChains: filtersData.club_chains.map(el => ({value: el.id, label: el.name})),
        sources: filtersData.sources.map(el => ({value: el.source, label: el.source})),

        sports: filtersData.sports.map(el => {
          //counting label-delimiters
          delimiterCount = el.label.split(".").length - 1;
          //insert delimiters
          name = el.name;
          for (let counter = 0; counter < delimiterCount; counter++) {
            name = '--' + name;
          }
          return ({
            value: el.id,
            label: name
          });
        }),

      }
      );


    filtersData.sports = filtersData.sports.sort((a, b) => {
      return a.label.localeCompare(b.label)
    });
    //delimiting
    let delimiterCount = null;
    let name = null;

    const params = [
      {name: 'source', method: this.state.sources},
      {name: 'city_id', method: this.state.cities},
      {name: 'club_chain_id', method: this.state.clubChains},
      {name: 'sport_id', method: this.state.sports}
    ];
    for (let param of params) {
      if (this.query_params.has(param.name)) {
        const apropriateClubIndex = param.method.findIndex((item) => item.value == this.query_params.get(param.name));
        let stateObj = {};
        stateObj[param.name] = param.method[apropriateClubIndex].value;
        this.setState(stateObj);
      }
    }
  };

  createEmptyObject(name = null, value = null) {
    let out = {};
    out['rank'] = "";
    out['city_id'] = "";
    out['club_chain_id'] = "";
    out['sport_id'] = "";
    out['name'] = "";
    out['source'] = "";
    out['sorted_field'] = "";
    out['sorted_order'] = "asc";
    return out;
  }

  createObject(name = null, value = null, mysorted = null) {
    let out = {};
    out['rank'] = this.state.rank;
    out['city_id'] = this.state.city_id;
    out['club_chain_id'] = this.state.club_chain_id;
    out['sport_id'] = this.state.sport_id;
    out['name'] = this.state.name;
    out['source'] = this.state.source;
    out['sorted_field'] = this.state.sortedField;
    out['sorted_order'] = mysorted ? "asc" : "desc";
    if (name) {
      out[name] = value;
    }
    console.log('out', out);
    return out;
  }

  filterCandge(value
                 :
                 string, field
                 :
                 string
  ) {
    const stateObj = {};
    stateObj[field] = value;
    this.setState(stateObj);
    this.child.bootstrap(this.createObject(field, value));
  }

  addSortedField(name) {
    let mySorted = !this.state.sortedAsk;
    if (name === this.state.sortedField) {
      //Если мы еще раз нажали, то туглим переключатель порядка
      this.setState({
        sortedAsk: !this.state.sortedAsk
      });

    } else {
      this.setState({
        sortedField: name,
        sortedAsk: true
      });
      mySorted = true
    }

    this.child.bootstrap(this.createObject('sorted_field', name, mySorted))
  }

  resetFilters() {
    this.setState({
      sortedField: "",
      sortedAsk: false,
      club_chain_id: "",
      source: "",
      sport_id: "",
      city_id: "",
      debName: "",
      name: "",
      rank: "",
    });
    history.push('/clubs');
    this.child.bootstrap(this.createEmptyObject())
  }

  getParrentHelperText(parent_club
                         :
                         {
                           id: number, name
                             :
                             string
                         }
  ) {
    return parent_club ? (
      <FormHelperText>
        Родитель: <Link to={`/clubs/${parent_club.id}/edit`} aria-label="Edit">{parent_club.name}</Link>
      </FormHelperText>
    ) : '';
  }

  render() {
    let {rank, club_chain_id, sortedField, sortedAsk, sport_id} = this.state;
    let buttonUp: React.ReactNode = <KeyboardArrowUp/>;
    let buttonDown: React.ReactNode = <KeyboardArrowDown/>;
    return (
      <React.Fragment>
        <H1>
          Клубы
        </H1>
        <Paper style={{
          overflowX: "auto",
          maxWidth: "78vw",
          minHeight: "78vh",
        }}>
          <Pagination url="/clubs" onRef={ref => (this.child = ref)}>
            {items => (
              <Table aria-labelledby="tableTitle">
                <TableHead>
                  <TableRow>
                    <ThinTableCell colSpan={2} padding="dense">
                      Ранг
                      <IconButton aria-label="Sorted" onClick={() => this.addSortedField('rank')}
                                  color={sortedField === "rank" ? "primary" : "default"}>
                        {sortedField === "rank" && !sortedAsk ? buttonUp : buttonDown}
                      </IconButton>
                    </ThinTableCell>
                    <TableCell>
                      Название
                      <IconButton aria-label="Sorted" onClick={() => this.addSortedField('name')}
                                  color={sortedField === "name" ? "primary" : "default"}>
                        {sortedField === "name" && !sortedAsk ? buttonUp : buttonDown}
                      </IconButton>
                    </TableCell>
                    <TableCell>
                      Сеть клубов
                    </TableCell>
                    <TableCell>
                      Количество залов
                    </TableCell>
                    <TableCell>
                      Последнее редактирование
                      <IconButton aria-label="Sorted" onClick={() => this.addSortedField('updated_at')}
                                  color={sortedField === "updated_at" ? "primary" : "default"}>
                        {sortedField === "updated_at" && !sortedAsk ? buttonUp : buttonDown}
                      </IconButton>
                    </TableCell>
                    <TableCell>
                      Город
                    </TableCell>
                    <TableCell>
                      Адрес
                    </TableCell>
                    <TableCell>
                      Виды спорта
                    </TableCell>
                    <TableCell>
                      <IconButton aria-label="Sorted" onClick={() => this.resetFilters()} color={"secondary"}>
                        <DeleteIcon/>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell colSpan={2} padding="dense">
                      <Select
                        style={{marginBottom: 0, marginTop: "34px"}}
                        suggestions={this.ranks}
                        value={this.state.rank}
                        margin="normal"
                        fullWidth
                        placeholder={"Ранг"}
                        onChange={(value) => this.filterCandge(value, 'rank')}
                      />
                    </TableCell>
                    <TableCell colSpan={1} padding="dense">
                      <Input
                        debounceLat={800}
                        value={this.state.debName}
                        change={(value) => {
                          this.filterCandge(value, 'name')
                        }}
                        label={"Название"}
                        margin="normal"
                        fullWidth
                      />
                    </TableCell>
                    <ExtendedTableCell colSpan={1} padding="dense" minWidth={200}>
                      <Select
                        style={{marginBottom: 0, marginTop: "34px"}}
                        suggestions={this.state.clubChains}
                        value={this.state.club_chain_id}
                        margin="normal"
                        fullWidth
                        placeholder={"Сеть клубов"}
                        onChange={(value) => this.filterCandge(value, 'club_chain_id')}
                      />
                    </ExtendedTableCell>
                    <TableCell colSpan={2} padding="dense">
                      <Select
                        style={{marginBottom: 0, marginTop: "34px"}}
                        suggestions={this.state.sources}
                        value={this.state.source}
                        margin="normal"
                        fullWidth
                        placeholder={"Источник"}
                        onChange={(value) => {
                          this.filterCandge(value, 'source')
                        }}
                      />
                    </TableCell>
                    <TableCell colSpan={2} padding="dense">
                      {console.log('this.cities', this.state.cities)}
                      <Select
                        style={{marginBottom: 0, marginTop: "34px"}}
                        suggestions={this.state.cities}
                        value={this.state.city_id}
                        margin="normal"
                        fullWidth
                        placeholder={"Город"}
                        onChange={(value) => this.filterCandge(value, 'city_id')}
                      />
                    </TableCell>
                    <TableCell colSpan={2} padding="dense">
                      <Select
                        style={{marginBottom: 0, marginTop: "34px"}}
                        suggestions={this.state.sports}
                        value={this.state.sport_id}
                        margin="normal"
                        fullWidth
                        placeholder={"Спорт"}
                        onChange={(value) => this.filterCandge(value, 'sport_id')}
                      />
                    </TableCell>
                  </TableRow>
                  {items.map(item => (
                    <TableRow
                      hover
                      key={item.id}
                    >
                      <ThinTableCell padding="dense">
                        <IconButtonLink to={`/clubs/${item.id}/edit`} aria-label="Edit">
                          <EditIcon/>
                        </IconButtonLink>
                      </ThinTableCell>
                      <ThinTableCell numeric padding="dense">
                        {item.rank}
                      </ThinTableCell>
                      <TableCell>
                        <Link to={`/clubs/${item.id}/edit`} aria-label="Edit">{item.name}</Link>
                        {this.getParrentHelperText(item.parent_club)}
                      </TableCell>
                      <TableCell>
                        {item.club_chain.name}
                      </TableCell>
                      <TableCell>
                        {item.playground_count}
                      </TableCell>
                      <TableCell>
                        {item.last_update}
                      </TableCell>
                      <TableCell>
                        {item.city}
                      </TableCell>
                      <TableCell>
                        {item.address}
                      </TableCell>
                      <TableCell colSpan={2}>
                        {this.getClubSports(item)}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </Pagination>
        </Paper>
        <AddButton to="/clubs/create"/>
      </React.Fragment>
    );
  }
}
;
