import * as React from "react";

import {BackButton} from "../../components/buttons";
import Loader from "../../components/loader";
import {H1, H2, H3} from "../../components/titles";
import {PaperPadded} from "../../components/paper";
import {apiGet} from "../../core/api";
import Form from "./form";
import {Club} from "../../models/club";
import {ListForClub} from "../playgrounds/list-for-club";
import OldSlugs from "../../components/old-slugs";
import {history} from "../../core/history";
import UploadImage from "../../components/upload-image";
import Grid from "@material-ui/core/Grid/Grid";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;

  state = {
    item: null,
    id: null,
    loading: true,
    form: null,
  }

  componentDidMount() {
    this.bootstrap();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  bootstrap = async () => {
    const {club} = this.props.match.params;
    this.setState({
      id: club,
    });
    try {
      const data = await apiGet(`/clubs/${club}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push('/clubs');
    }
  };

  onSubmit = (saved: Club) => {
    console.log(saved);
    this.setState({
      item: new Club(saved),
    });
  };


  render() {
    const {id, loading, item} = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/clubs"/>
          Клуб №{id} {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
              <Grid>
                <Form item={item} onSubmitted={this.onSubmit}/>
                <PaperPadded style={{marginTop:50}}>
                  <OldSlugs slugs={item.slugs}></OldSlugs>
                </PaperPadded>
              </Grid>


          )}
        </Loader>
        {item && <UploadImage url={`/clubs/${item.id}`} image={item.logo} field="logo" title="Логотип"/>}
        {item && <UploadImage url={`/clubs/${item.id}`} image={item.images} field="image[]" title="Фото" isMulti/>}
        {id && <ListForClub id={id}/>}

      </React.Fragment>
    );
  }
}
