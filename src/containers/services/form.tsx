import * as React from "react";
import * as _ from "lodash";

import Submit from "../../components/form/submit";
import Input from "../../components/form/input-control";
import AbstractForm from "../abstract-form";

export default class Form extends AbstractForm
{

  apiUri = '/services/';

  render() {
    const { item } = this.props;
    const { form, draft, busy, errors, message } = this.state

    return (form &&
      <form>
        <Input
          label="ЧПУ"
          value={form.slug}
          change={this.handleChange('slug')}
          margin="normal"
          fullWidth
          disabled={true}
          errorText={ this.fieldError('slug') }
          helperText={ 'Генерируется автоматически' }
        />
        
        <Input
          label="Название"
          value={form.name}
          change={this.handleChange('name')}
          margin="normal"
          fullWidth
          disabled={busy}
          errorText={ this.fieldError('name') }
        />
        
        <Input
          label="Текстовое описание"
          value={form.description}
          change={this.handleChange('description')}
          margin="normal"
          fullWidth
          disabled={busy}
          multiline
          rows={5}
          errorText={ this.fieldError('description') }
        />

        <Submit  
          float
          onSubmit={this.submit} 
          disabled={_.isEqual(form, item)} 
          loading={busy}
          message={message} 
        />
      </form>
    );
  }
}