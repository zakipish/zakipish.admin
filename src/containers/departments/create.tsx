import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import { history } from "../../core/history";
import { Department } from "../../models/department";

export class Create extends React.Component<{match:any}> {

  state = {
    item: new Department(),
  }

  onSubmit = (saved: Department) => {
    const {city} = this.props.match.params;
    history.push(`/cities/${city}/edit/`);
  };

  render() {
    const { item } = this.state;

    const { city } = this.props.match.params;
    return (
      <React.Fragment>
        <H1>
          <BackButton to={`/cities/${city}/edit`} />
          Создание Района
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} match={this.props.match} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
