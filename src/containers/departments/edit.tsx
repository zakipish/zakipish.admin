import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1, H2 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import {apiDelete, apiGet} from "../../core/api";
import Form from "./form";
import OldSlugs from "../../components/old-slugs";
import { history } from "../../core/history";
import { Department } from "../../models/department";
import DeleteIcon from '@material-ui/icons/Delete';
import { Button} from "@material-ui/core";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;
  
  state = {
    item: null,
    id: null,
    loading: true,
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }
  deleteDepartment(id){
    if(confirm("Вы точно хотите удалить район?")){
      let result = apiDelete(`/departments/${id}`);
      if (result){
        const { city } = this.props.match.params;
        history.push(`/cities/${city}`);
      }
    }

  }
  bootstrap = async () => {
    const { department,city } = this.props.match.params;
    this.setState({
      id: department,
      cityId: city,
    });
    try {
      console.log(department)
      const data = await apiGet(`/departments/${department}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push(`/cities/${city}`);
    }
  };

  onSubmit = (saved: Department) => {
    this.setState({
      item: new Department(saved),
    });
    const { city } = this.props.match.params;
    history.push(`/cities/${city}`);
  };

  render() {
    const { id, loading, item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to={`/cities/${this.props.match.params.city}/edit`} />
          Район {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
              <div>
                <PaperPadded>
                  <Form item={item} match={this.props.match} onSubmitted={this.onSubmit} />
                </PaperPadded>
                <Button variant="contained" color="secondary" size="large" style={{marginTop:"15px",marginRight :"15px"}} onClick={()=>this.deleteDepartment(item.id)}>
                  Удалить
                  <DeleteIcon />
                </Button>
              </div>

          )}

        </Loader>

      </React.Fragment>
    );
  }
}
