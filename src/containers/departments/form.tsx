import * as React from "react";
import * as _ from "lodash";

import Submit from "../../components/form/submit";
import Input from "../../components/form/input-control";
import AbstractForm from "../abstract-form";
import {apiGet} from "../../core/api";
import Select from "../../components/form/select-control";

export default class Form extends AbstractForm
{

  cities=null;
  apiUri = '/departments/';
  render() {
    const { item } = this.props;
    const { city } = this.props.match.params;
    const { cities } = this;
    const { form, busy, errors, message } = this.state;
    form.city_id =city;
    return (form &&
      <form>
        <Input
          label="Название"
          value={form.name}
          change={this.handleChange('name')}
          margin="normal"
          fullWidth
          disabled={busy}
          errorText={ this.fieldError('name') }
        />

        <Submit  
          float
          onSubmit={this.submit} 
          disabled={_.isEqual(form, item)} 
          loading={busy}
          message={message} 
        />
      </form>
    );
  }
}