import * as React from "react";

import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper } from "@material-ui/core";

import {IconButtonLink, AddButton, ButtonLink} from "../../components/buttons";
import {H1, H2Flex} from "../../components/titles";
import Pagination from "../../components/pagination";
import { ThinTableCell } from "../../components/table";

export class ListForCity extends React.Component<{cityId:number}> {
  render() {
      const {cityId} = this.props;
    return (
      <React.Fragment>
          <H2Flex>
              <span>Районы</span>
              <ButtonLink
                  variant="contained"
                  size="large"
                  color="primary"
                  to={`/cities/${cityId}/departments/create`}
              >
                  Добавить район
              </ButtonLink>
          </H2Flex>
        <Paper>
          <Pagination url={`/departments?city_id=${cityId}`}>
            { items => (
              <Table aria-labelledby="tableTitle">
                <TableHead>
                  <TableRow>
                    <ThinTableCell padding="dense">
                      Id
                    </ThinTableCell>
                    <TableCell>
                      Название
                    </TableCell>
                    <TableCell>
                      ЧПУ
                    </TableCell>
                    <ThinTableCell  padding="none">
                    </ThinTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map(item => (
                    <TableRow
                      hover
                      key={item.id}
                    >
                      <ThinTableCell numeric padding="dense">
                        {item.id}
                      </ThinTableCell>
                      <TableCell>
                        {item.name}
                      </TableCell>
                      <ThinTableCell padding="none">
                        <IconButtonLink to={`/cities/${cityId}/departments/${item.id}/edit`} aria-label="Редактировать">
                          <EditIcon />
                        </IconButtonLink>
                      </ThinTableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </Pagination>
        </Paper> 
        <AddButton to="/departments/create" />
      </React.Fragment>
    );
  }
};
