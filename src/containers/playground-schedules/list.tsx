import * as React from "react";
import moment from 'moment';

import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper } from "@material-ui/core";

import {IconButtonLink, ButtonLink, BackButton} from "../../components/buttons";
import { H2Flex } from "../../components/titles";
import Pagination from "../../components/pagination";
import { ThinTableCell } from "../../components/table";

export class List extends React.Component<{
  id: Number,
  match:{params:{playground:number,club:number}}
}> {

  getDateOrInfinity = (date) => {
    return date
    ? moment(date, moment.HTML5_FMT.DATETIME_LOCAL_SECONDS).format('DD.MM.YYYY HH:mm')
    : '∞'
  }

  renderDuration = (duration) => {
    if (!duration) {
      return '∞—∞';
    }
    return this.getDateOrInfinity(duration[0]) + '—' + this.getDateOrInfinity(duration[1]);
  }

  render() {
    const { playground, club } = this.props.match.params;
    return (
      <React.Fragment>
        <H2Flex>

          <span><BackButton to={`/clubs/${club}/playgrounds/${playground}/edit`} />Список цен</span>
          <ButtonLink
            variant="contained"
            color="primary"
            to={`/clubs/${club}/playgrounds/${playground}/playground-schedule/create`}
          >
            Добавить цену
          </ButtonLink>
        </H2Flex>
        <Paper>
          <Pagination url={`/playground-schedules?playground_id=${playground}`}>
            {items => (
              <Table aria-labelledby="tableTitle">
                <TableHead>
                  <TableRow style={{height:"34px"}}>
                    <ThinTableCell padding="dense">
                      Цена
                    </ThinTableCell>
                    <TableCell>
                      Дата
                    </TableCell>
                    <TableCell>
                      Время
                    </TableCell>
                    <ThinTableCell  padding="none">
                    </ThinTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map(item => (
                    <TableRow
                      style={item.deleted_at?{background:"#aaaaaa"}:{}}
                      hover
                      key={item.id}
                    >
                      <ThinTableCell numeric padding="dense">
                        {item.price}
                      </ThinTableCell>
                      <TableCell>
                        {item.date ? moment(item.date).format('D MMM YY') : ''}
                      </TableCell>
                      <TableCell>
                        {item.interval ? item.interval.join('—') : ''}
                      </TableCell>
                      <ThinTableCell padding="none">
                        <IconButtonLink to={`/clubs/${club}/playgrounds/${playground}/playground-schedule/${item.id}/edit`} aria-label="Edit">
                          <EditIcon />
                        </IconButtonLink>
                      </ThinTableCell>

                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </Pagination>
        </Paper>
      </React.Fragment>
    );
  }
};
