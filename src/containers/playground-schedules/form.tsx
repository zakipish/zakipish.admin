import * as React from "react";
import * as _ from "lodash";
import { Grid, Divider } from "@material-ui/core";

import Submit from "../../components/form/submit";
import AbstractForm from "../abstract-form";
import RadioButtonField from "../../components/form/RadioButtonField";
import DateIntervalControl from "../../components/form/DateIntervalControl";
import Input from "../../components/form/input-control";
import DateControl from "../../components/form/DateControl";
import SelectWeekControl from "../../components/form/SelectWeekControl";
import {apiGet} from "../../core/api";

export default class Form extends AbstractForm<{}, {
  price?: number;
  interval?:[string, string];
  min_interval?: number;
}> {
  apiUri = '/playground-schedules/';

  constructor(props){
    super(props);
    this.getPlaygroundInfo();
  }
  getPlaygroundInfo = async () =>{
    const playgroundInfo = await apiGet(`/playgrounds/${this.state.form.playground_id}`, null, true);
    !this.unmounted && this.setState({
      min_interval: playgroundInfo.data.min_interval,
    });
  }

  render() {
    const { item } = this.props;
    const { form, busy, message, min_interval } = this.state;
    return (
      <form>
        <Grid container spacing={16}>
          <Grid item xs={12} sm>
              <DateControl
                type='date'
                margin="normal"
                fullWidth
                label="Дата"
                value={form.date}
                onChange={this.handleChange('date')}
                disabled={busy}
                errorText={this.fieldError('date')}
              />
          </Grid>
        </Grid>

        <Input
          label="Цена"
          margin="normal"
          fullWidth
          type="number"
          value={form.price}
          change={this.handleChange('price')}
          disabled={busy}
          errorText={this.fieldError('price')}
          helperText={(min_interval) ? 'Цена указывается за ' + min_interval + ' мин.' : 'Временной период у зала не указан.' }
        />

        <DateIntervalControl
          type="time"
          label="Время работы"
          margin="normal"
          fullWidth
          value={form.interval}
          onChange={this.handleChange('interval')}
          disabled={busy}
          errorText={this.fieldError('interval')}
        />


        <Divider />

        <Submit
          float
          onSubmit={this.submit}
          disabled={_.isEqual(form, item)}
          loading={busy}
          message={message}
        />
      </form>
    );
  }
}
