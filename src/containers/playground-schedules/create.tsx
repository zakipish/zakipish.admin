import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import Form from "./form";
import { PlaygroundSchedule } from "../../models/playground-schedule";
import { history } from "../../core/history";

import FormLabel from '@material-ui/core/FormLabel';
export class Create extends React.Component<{
  match: any;
}> {

  state = {
    item: null,
    clubId: null,
    playground:null,
  }

  constructor(props) {
    super(props);
    const { club, playground } = this.props.match.params;
    this.state = {
      item: new PlaygroundSchedule({ playground_id: playground }),
      clubId: club,
      playground: playground
    };
  }

  onSubmit = (saved: PlaygroundSchedule) => {
    const { club, playground, schedule } = this.props.match.params;
    history.push(`/clubs/${club}/playgrounds/${playground}/playground-schedules`);
  };

  render() {
    const { item, clubId,playground } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to={`/clubs/${clubId}/playgrounds/${playground}/playground-schedules`} />
          Добавление цены
        </H1>
        <Loader loading={false}>
          <PaperPadded>
            <Form item={item} onSubmitted={this.onSubmit} />
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
