import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import Form from "./form";
import { PlaygroundSchedule } from "../../models/playground-schedule";
import { history } from "../../core/history";

import FormLabel from '@material-ui/core/FormLabel';
import {apiGet} from "../../core/api";
export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;
  state = {
    item: null,
    clubId: null,
    playground:null,
    schedule:null,
    loading: true,
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.bootstrap();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }
  
  bootstrap = async () => {
    const { playground, club,schedule } = this.props.match.params;
    this.setState({
      id: playground,
      clubId: club,
      playground:playground
    });
    try {
      const data = await apiGet(`/playground-schedules/${schedule}`);
      !this.unmounted && this.setState({
        item: data.data,
        loading: false,
      });
    } catch (e) {
      history.push(`/clubs/${club}/playgrounds/${playground}/playground-schedules/`);
    }
  };

  onSubmit = (saved: PlaygroundSchedule) => {
    const { clubId,item,playground,schedule } = this.state;
    history.push(`/clubs/${clubId}/playgrounds/${playground}/playground-schedules/`);
  };

  render() {
    const { item, clubId,playground,loading } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to={`/clubs/${clubId}/playgrounds/${playground}/playground-schedules`} />
          Редактирование цены
        </H1>
        <Loader loading={loading}>
          <PaperPadded>
            {item && <Form item={item} onSubmitted={this.onSubmit} />}
          </PaperPadded>
        </Loader>
      </React.Fragment>
    );
  }
}
