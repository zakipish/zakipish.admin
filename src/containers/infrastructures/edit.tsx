import * as React from "react";

import { BackButton } from "../../components/buttons";
import Loader from "../../components/loader";
import { H1, H2 } from "../../components/titles";
import { PaperPadded } from "../../components/paper";
import { apiGet } from "../../core/api";
import Form from "./form";
import OldSlugs from "../../components/old-slugs";
import { history } from "../../core/history";
import { Infrastructure } from "../../models/infrastructure";
import UploadImage from "../../components/upload-image";

export class Edit extends React.Component<{
  match: any;
}> {
  unmounted = false;
  
  state = {
    item: null,
    id: null,
    loading: true,
  }

  componentDidMount() {
    this.bootstrap();
  }
  componentWillUnmount() {
    this.unmounted = true;
  }

  bootstrap = async () => {
    const { infrastructure } = this.props.match.params;
    this.setState({
      id: infrastructure,
    });
    try {
      const data = await apiGet(`/infrastructures/${infrastructure}`);
      !this.unmounted && this.setState({
        item: data.data,
        form: {...data.data},
        loading: false,
      });
    } catch (e) {
      history.push('/infrastructures');
    }
  };

  onSubmit = (saved: Infrastructure) => {
    this.setState({
      item: new Infrastructure(saved),
    });
    history.push('/infrastructures');
  };

  render() {
    const { id, loading, item } = this.state;
    return (
      <React.Fragment>
        <H1>
          <BackButton to="/infrastructures" />
          Инфраструктура №{id} {item ? `"${item.name}"` : ''}
        </H1>
        <Loader loading={loading}>
          {item && (
            <PaperPadded>
              <Form item={item} onSubmitted={this.onSubmit} />
              <OldSlugs slugs={item.slugs}></OldSlugs>
            </PaperPadded>

          )}
        </Loader>
        {item && <UploadImage url={`/infrastructures/${item.id}`} image={item.icon} field="icon" title="Логотип"/>}
      </React.Fragment>
    );
  }
}
