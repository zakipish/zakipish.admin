import * as React from "react";
import * as _ from "lodash";
import moment from 'moment';


import {apiPatch, apiPost} from "../core/api";
import {notificate} from "../components/notification/notificationManager"

interface AbstractFormProps {
  item: any,
  match?:any,
  onSubmitted: (saved: any) => void,
}

export interface AbstractFormState {
  form?: any;
  draft?: any;
  busy?: boolean;
  errors?: any;
  message?: string;
}

export default abstract class AbstractForm<P = {}, S = {}>
  extends React.Component<AbstractFormProps & P, AbstractFormState & S> {
  apiUri;
  unmounted = false;

  state: any = {};

  constructor(props) {
    super(props);
    this.state.busy = false;
    this.state.form = this.cloneModel(props.item);
  }

  cloneModel = (model) => {
    return _.cloneDeep(model);
  };

  componentWillUnmount() {
    this.unmounted = true;
  };

  handleChange = (name: string) => (value: any) => {
    const {form} = this.state;
    form[name] = value;
    this.setState({
      form,
    });
    // console.log(form)
  };
  handleSportsChange = (value: any) => {
    const {form,sports} = this.state;
    let current_sport = null;
    let current_value = value[value.length -1 ];
    let current_parents = null;
    let result = value;
    let missing_indexs = form['sports']?form['sports'].filter(item => result.indexOf(item) === -1):[];
    console.log(missing_indexs)
    if (missing_indexs.length === 0){
      for (let item of sports){
        if (item.value === current_value){
          current_sport = item;
          break;
        }
      }
      if (current_sport){
        current_parents = current_sport.parents.split(".");
        if (current_parents){
          for (let parent of current_parents){
            let parent_ind = parseInt(parent)
            if (result.indexOf(parent_ind) === -1)
              result.push(parent_ind)
          }
        }
      }
    } else {
      let out = [];
      for (let missing_index of missing_indexs) {
        out = out.concat(sports.filter(item => item.parents.indexOf(missing_index) === -1 && form['sports'].indexOf(item.value) !== -1))
      }
      result = out.map(item => item.value );
    }
    form['sports'] = result? result: [];
    this.setState({
      form,
    });

  }

  submit = async () => {
    const {form} = this.state;
    const {item, onSubmitted} = this.props;
    this.setState({
      busy: true,
      errors: null,
      message: null,
      draft: this.cloneModel(form),
    });
    const body = JSON.stringify(form);
    // console.log(body);
    try {
      const data = item.id ?
        await apiPatch(`${this.apiUri}${item.id}`, body) :
        await apiPost(this.apiUri, body);
      onSubmitted(data.data);
      notificate('SAVED');
      !this.unmounted && this.setState({
        form: this.cloneModel(data.data),
      });

    } catch (e) {
      // console.log(e)
      !this.unmounted && this.setState({
        errors: e.errors,
        message: e.message,
      });
    }
    !this.unmounted && this.setState({
      busy: false,
    });
  };

  fieldError = (key: string) => {
    const {errors, form, draft} = this.state
    // console.log(key, errors);
    return errors && errors[key] && _.isEqual(draft[key], form[key]) ? errors[key] : null;
  };

}