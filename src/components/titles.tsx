import * as React from "react";
import Typography from '@material-ui/core/Typography';
import {withStyles} from "@material-ui/core";

export const H1: React.StatelessComponent = ({children}) => {
  return (
    <Typography variant="display2" component="h1" gutterBottom>
      {children}
    </Typography>
  );
};

export const H2: React.StatelessComponent = ({children}) => {
  return (
    <Typography variant="display1" component="h2" gutterBottom>
      {children}
    </Typography>
  );
};

export const H3: React.StatelessComponent = ({children}) => {
  return (
    <Typography variant="title" component="h4">
      {children}
    </Typography>
  );
};
export const H3Contact: React.StatelessComponent = ({children}) => {
  return (
    <Typography style={{textAlign: "left", marginTop: "-26px"}} variant="title" component="h4">
      {children}
    </Typography>
  );
};
export const H3Geo: React.StatelessComponent = ({children}) => {
  return (
    <Typography style={{marginBottom: "16px"}} variant="title" component="h4">
      {children}
    </Typography>
  );
};
const H2FlexComponent: React.StatelessComponent<{
  classes: any,
}> = ({children, classes}) => {
  return (
    <Typography
      variant="display1"
      component="h2"
      gutterBottom
      className={classes.root}
    >
      {children}
    </Typography>
  );
};

export const H2Flex = withStyles(theme => ({
  root: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
}))(H2FlexComponent);