import React from "react";
import { ListItemText, ListItem, Button, Collapse, withStyles } from "@material-ui/core";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import NestedListItemInterface from "./nested-list-item-interface";
import NestedList from "./nested-list";
import EditIcon from '@material-ui/icons/Edit';
import DndTarget from "./dnd-target";
import DndSource from "./dnd-source";

const styles = theme => ({
  nestedZone: {
    paddingLeft: theme.spacing.unit * 4,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
    paddingBottom: theme.spacing.unit * 1,
    marginBottom: theme.spacing.unit * 1,
    borderLeft: "1px solid ".concat(theme.palette.divider),    
    borderBottom: "1px solid ".concat(theme.palette.divider),    
  },
  spaceForButton: {
    width: 64,
  }
});


class NestedItemComponent extends React.Component<{
  classes: any,
  index: number,
  root: NestedListItemInterface,
  item: NestedListItemInterface,
  addItem: (item:NestedListItemInterface) => void,
  editItem: (item:NestedListItemInterface) => void,
  removeItem: (item:NestedListItemInterface, root:NestedListItemInterface) => void,
  onToggle: (item:NestedListItemInterface) => void,
  onMove: (sourceList, oldIndex, targetList, newIndex) => void,
}>
{

  render() {
    const { index, root, item, classes, addItem, removeItem, editItem, onMove, onToggle } = this.props;
    return (
      <React.Fragment>
        <DndSource index={index} root={root}>
          <ListItem disableGutters>
            <ListItemText primary={item.name} />
            <Button mini onClick={() => editItem(item)}>
              <EditIcon />
            </Button>
            <Button mini onClick={() => addItem(item)}>
              <AddIcon />
            </Button>
            <Button mini onClick={() => removeItem(item, root)}>
              <DeleteIcon />
            </Button>
            {item.items.length ? (
              <Button mini onClick={() => onToggle(item)}>
                {item.open ? <ExpandLess /> : <ExpandMore />}
              </Button>
            ) : (
              <div className={classes.spaceForButton} />
            )}
          </ListItem>
        </DndSource>
        {item.items.length ? (
          <Collapse in={item.open} timeout="auto" unmountOnExit>
            <div className={classes.nested}>
              <NestedList 
                root={item} 
                addItem={addItem} 
                removeItem={removeItem} 
                editItem={editItem} 
                onMove={onMove} 
                onToggle={onToggle}
              />
            </div>
          </Collapse>
        ) : null}
        {item.items.length === 0 || !item.open ? (
          <div className={classes.nestedZone}>
            <DndTarget 
              up
              onMove={onMove} 
              root={item}
              index={0}
            />
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}
const NestedItem = withStyles(styles)(NestedItemComponent);
export default NestedItem;

