import React from "react";
import { List } from "@material-ui/core";
import NestedListItemInterface from "./nested-list-item-interface";
import NestedItem from "./nested-item";
import DndTarget from "./dnd-target";


class NestedList extends React.Component<{
  root: NestedListItemInterface,
  addItem: (item:NestedListItemInterface) => void,
  editItem: (item:NestedListItemInterface) => void,
  onToggle: (item:NestedListItemInterface) => void,
  removeItem: (item:NestedListItemInterface, root:NestedListItemInterface) => void,
  onMove: (sourceList:NestedListItemInterface, oldIndex:number, targetList:NestedListItemInterface, newIndex:number) => void,
}>
{
  render() {
    const { root, addItem, removeItem, editItem, onMove, onToggle } = this.props;
    return (
      <List component="div" disablePadding>
        <DndTarget 
          onMove={onMove} 
          root={root}
          index={0}
        />
        {root.items.map((item, i) => (
          <React.Fragment key={i}>
            {/* <DndSource index={i} root={root}> */}
              <NestedItem
                index={i}
                root={root}
                item={item} 
                addItem={addItem}
                removeItem={removeItem}
                editItem={editItem}
                onMove={onMove}
                onToggle={onToggle}
              />
            {/* </DndSource> */}
            <DndTarget
              onMove={onMove}
              root={root}
              index={i + 1}
            />
          </React.Fragment>
        ))}
      </List>
    );
  }
}

export default NestedList;
