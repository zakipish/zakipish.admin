export default interface NestedListItemInterface {
  id: number;
  name: string;
  items: NestedListItemInterface[];
  open: boolean;
}
