
import * as React from 'react';
import {
  ConnectDropTarget,
  DropTarget,
  DropTargetConnector,
  DropTargetMonitor,
} from 'react-dnd';
import NestedListItemInterface from './nested-list-item-interface';

const TYPE = 'card';

interface Props {
  index: number,
  root: NestedListItemInterface,
  children?: any,
  up?: boolean,
  onMove: (sourceList:NestedListItemInterface, oldIndex:number, targetList:NestedListItemInterface, newIndex:number) => void,
  isOver?: boolean,
  canDrop?: boolean,
  connectDropTarget?: ConnectDropTarget;
}

const DndTargetConponent = (props: Props) => {
  const { up, isOver, canDrop, connectDropTarget } = props;
  return connectDropTarget(
    <div 
      style={{
        zIndex: canDrop ? 1 : 0,
        display: canDrop ? 'block' : 'none',
        position: 'relative',
        height: isOver ? 48 : 24,
        transition: 'all .1s',
        marginBottom: up ? 0 : -12,
        marginTop: up ? -24 : -12,
        paddingTop: 12,
        paddingBottom: 12,
      }}
    >
      <div 
        style={{
          height: isOver ? '100%' : 0,
          background: '#ccffdd',
          border: isOver ? '1px dashed grey' : '',
        }}
      />
    </div>
  );
}


const dropSpec = {
  drop(props: Props, monitor: DropTargetMonitor) {
    const { index, root } = props;
    const source = monitor.getItem();
    props.onMove && props.onMove(source.root, source.index, root, index);
  },
  canDrop(props, monitor: DropTargetMonitor) {
    const { index, root } = props;
    const source = monitor.getItem();
    if (source.root === root 
      && (source.index === index || source.index + 1 === index)) {
        // На место себя
        return false;
    }
    if (source.root.items[source.index] === root) {
      return false;
    }
    return true;
  }
};

// Returns the functions needed to connect the source DOM node to the React DnD backend.

const dropCollect = (connect: DropTargetConnector, monitor: DropTargetMonitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
});

const DndTarget = DropTarget(TYPE, dropSpec, dropCollect)(DndTargetConponent);
export default DndTarget;