
import * as React from 'react';
import {
  DragElementWrapper,
  DragSource,
  DragSourceConnector,
  DragSourceOptions,
  DragSourceMonitor,
} from 'react-dnd';
import NestedListItemInterface from './nested-list-item-interface';


const TYPE = 'card';

interface Props {
  index: number;
  root: NestedListItemInterface;
  children?: any,
  isDragging?: boolean,
  connectDragSource?: DragElementWrapper<DragSourceOptions>;
}

const DndSourceConponent = (props: Props) => {
  const { isDragging, connectDragSource, children } = props;
  return connectDragSource(
      <div 
        style={{
          opacity: isDragging ? .5 : 1,
          cursor: 'move',
        }}
      >
        {children}
      </div>
  );
}

const dragSpec = {
  // Returns an object describing the item being dragged.
  beginDrag(props: Props) {
    const { index, root } = props
    return {index, root};
  },
};

// Returns the functions needed to connect the source DOM node to the React DnD backend.
const dragCollect = (connect: DragSourceConnector, monitor: DragSourceMonitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
});


const DndSource = DragSource(TYPE, dragSpec, dragCollect)(DndSourceConponent);
export default DndSource;