import * as React from "react";
import Typography, { TypographyProps } from '@material-ui/core/Typography';
import { withStyles } from "@material-ui/core";

export const P:React.StatelessComponent<TypographyProps> = ({children, ...props}) => {
  return (
    <Typography variant="body2" component="p" gutterBottom {...props}>
      {children}
    </Typography>
  );
};