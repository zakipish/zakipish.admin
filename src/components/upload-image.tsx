import * as React from "react";
import {FormControl, Button, Card, CircularProgress, Grid, Modal, TextField} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles';
import {H2Flex} from "./titles";
import {PaperPadded} from "./paper";
import {apiPost} from "../core/api";
import {P} from "./p";
import Error from "./form/error";
import {AssignableObject} from "../core/utils";
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import ClearIcon from '@material-ui/icons/Clear';

interface UploadedImage {
    id: Number,

    order: number;
    url: string
}

class Image extends AssignableObject {
    id: number;
    order: number;
    preview: string;
    file: File;
    loading: boolean;
}

const ImageBlockComponent: React.StatelessComponent<{
        classes: any,
        image: Image,
        index: number
        onDelete: any,
    }> = ({classes, image, onDelete}) => (

        <Card className={classes.preview} style={{zIndex: 1}}>
            <img className={classes.img} src={image.preview}/>
            {image.loading &&
            <div className={classes.progressWrapper}><CircularProgress size={24} className={classes.buttonProgress}
                                                                       color="secondary"/></div>}
        </Card>
    ),
    ImageBlock = withStyles(theme => ({
        preview: {
            maxWidth: 100,
            maxHeight: 100,
            position: 'relative',
            zIndex: 1
        },
        buttonProgress: {
            // color: green[500],
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: -12,
            marginLeft: -12,
        },
        progressWrapper: {
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: "rgba(255,255,255,0.5)",
        },
        img: {
            display: 'block',
            width: '100%',
        },
    }))(ImageBlockComponent);

interface UploadImagetate {
    previews: Image[],
    error: string,
    showDeleteIcon: boolean,
    showUrlModal: boolean,
    imageUrl: string,
}

//Sortable adding
const SortableImageBlock = SortableElement(ImageBlock);
const NewPaperPadded: React.StatelessComponent<{
    classes: any,
    isMulti: boolean,
    images: any,
    error: any,
    onDelete: any,
    axis: any,
    showDeleteIcon: boolean,
}> = ({classes, isMulti, images, error, onDelete, axis, showDeleteIcon}) => (
    <PaperPadded className={classes.paper}>
        {
            images.map((image, i) => (
                <div className={classes.paperContent} key={i}>
                    {showDeleteIcon &&
                    <ClearIcon className={classes.icon}
                               color="secondary"
                               fontSize="small"
                               onClick={() => onDelete(image)}
                    />
                    }

                    <SortableImageBlock key={i} index={i} image={image} onDelete={onDelete}/>
                </div>
            ))}
        {images.length + images.length === 0 && <P align="center">Изображения нет</P>}
        {error && <Error className={classes.message} message={error}/>}
        <div className={classes.clear}/>
    </PaperPadded>
);
const SortabelPaperPadded = SortableContainer(NewPaperPadded);

//
class UploadImage extends React.Component

    <{
        classes: any,
        url
            :
            string,
        field
            :
            string,
        title
            :
            string,
        image?: UploadedImage | UploadedImage[],
        isMulti?: boolean,
        delete_action?: string
    }> {

    state: UploadImagetate = {
        previews: [],
        error: null,
        showDeleteIcon: true,
        showUrlModal: false,
        imageUrl: null,
    };

    createImageFromFile = (file: File) =>
        new Image({
            file: file,
            preview: URL.createObjectURL(file),
            loading: true,
        });

    handleImage = async (e) => {
        const {field, isMulti} = this.props;
        let {previews} = this.state,
            newImages = [],
            newPreviews = [];

        if (!e.target.files.length) {
            return;
        }
        if (isMulti) {
            for (let i = 0; i < e.target.files.length; i++) {
                newImages.push(this.createImageFromFile(e.target.files[i]))
            }
            newPreviews = [...previews, ...newImages];
        } else {
            newImages = [this.createImageFromFile(e.target.files[0])];
            newPreviews = newImages;
        }
        this.setState({
            previews: newPreviews,
            error: null,
        });

        let form = new FormData();
        let post_data;
        form.append('_method', 'PATCH');
        newImages.forEach(image => {
            console.log('image appended', image.file.name)
            form.append(field, image.file, image.file.name);
        });
        try {
            post_data = await apiPost(this.props.url, form);
            newImages.forEach((image, index) => {
                image.loading = false;
                if (field == "image[]") {
                    image.id = post_data[index].id;
                    image.order = post_data[index].order;
                }
            });
        } catch (e) {
            this.setState({
                error: e.message || 'Some error',
            })
        }

        this.setState({
            previews: [...newPreviews],
        });
    };

    removeImage = () => {
        this.setState({
            image: null,
            preview: null,
        });
    };

    deletePreviewImage = (i: Number) => {

    };

    deleteUploadedImage = (image: Image) => {
        const { delete_action } = this.props
        let form = new FormData();
        if (delete_action != null) {
            form.append('action', delete_action)
        } else {
            if (this.props.isMulti) {
                form.append('action', 'delete_image');
            } else {
                form.append('action', 'delete_logo');
            }
        }
        form.append('_method', 'PATCH');
        //@ts-ignore
        form.append('id', image.id.toString());
        let result = apiPost(this.props.url, form);
        if (result) {
            let newPreviews: Image[] = this.state.previews.filter(item => item.id !== image.id);
            this.setState({
                    previews: newPreviews
                }
            );

        }
    };

    getImagesArray = (image: UploadedImage | UploadedImage[]) => {

        if (!image) {
            return [];
        }
        if (Array.isArray(image)) {
            return image.map(image => {
                return new Image({
                    preview: image.url,
                    order: image.order,
                    id: image.id

                });
            });
        }
        return [new Image({
            preview: image.url,
            id: image.id,
        })];

    };

    onSortStart = () => {
        this.setState({showDeleteIcon: false});

    };

    onSortEnd = ({oldIndex, newIndex}) => {
        this.setState({
            previews: arrayMove(this.state.previews, oldIndex, newIndex),
            showDeleteIcon: true,
        });
        console.log(this.state.showDeleteIcon);

        let form = new FormData();
        form.append('action', 'update_images_order');
        form.append('_method', 'PATCH');
        this.state.previews.forEach(image => {
            form.append("images[]", image.preview);
            // @ts-ignore
            form.append("image_ids[]", image.id.toString());
        });
        apiPost(this.props.url, form);
    };


    componentDidMount() {
        const images: Image[] = this.getImagesArray(this.props.image);
        let fullImages = images.concat(this.state.previews);
        this.setState({
            previews: fullImages.sort((a, b) => {
                if (a.order < b.order)
                    return -1;
                if (a.order > b.order)
                    return 1;
                return 0
            }),
        });

    }

    //show modal for images form irl
    handleUrlModalOpen = () => {
        this.setState({showUrlModal: true});
    };

    //close modal for images form irl
    handleUrlModalClose = () => {
        this.setState({showUrlModal: false});
    };

    //check URL link
    changeUrl = (e) => {
        this.setState({imageUrl: e.target.value});

    }

    //here link go to the back-end
    handleImageUrl = async () => {
        console.log(this.props)
        console.log(this.state)
        const {field} = this.props;
        let {previews} = this.state;
        let form = new FormData();

        form.append('_method', 'PATCH');
        form.append('imageUrl', this.state.imageUrl);
        form.append('field', field);
        let newPreview;
        try {
            let promise = await apiPost(this.props.url, form);
            newPreview = promise
        } catch (e) {
            this.setState({
                error: e.message || 'Some error',
            })
        }
        this.handleUrlModalClose();
        let image = new Image({
            id: newPreview[0].id,
            order: newPreview[0].order,
            preview: newPreview[0].preview,
        });
        if (field === 'logo') {
            this.setState({
                previews: [image],
            });
        } else {
            this.setState({
                previews: [...previews, image],
            });
        }
    }

    render() {
        const {classes, isMulti, image, title, field} = this.props;
        const {error} = this.state;
        return (
            <React.Fragment>
                <H2Flex>
                    <FormControl margin="normal">
                        <span>{title}</span>
                    </FormControl>
                    <FormControl margin="normal">

                        <Grid container spacing={8}>
                            <Grid item>
                                <Button
                                    component="span"
                                    size="large"
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleUrlModalOpen}
                                >
                                    Загрузить из URL
                                </Button>
                                <Modal
                                    aria-labelledby="simple-modal-title"
                                    aria-describedby="simple-modal-description"
                                    open={this.state.showUrlModal}
                                    onClose={this.handleUrlModalClose}
                                >

                                    <Grid className={classes.modal}>
                                        <Grid container className={classes.modalItem} style={{width: "100%"}}
                                              spacing={8}>
                                            <Grid item>
                                                <TextField
                                                    id={`imageUrl-${field}`}
                                                    label="URL"
                                                    onChange={this.changeUrl}
                                                >
                                                </TextField>
                                            </Grid>
                                        </Grid>
                                        <Grid container className={classes.modalItem} spacing={8}>
                                            <Grid item>
                                                <Button
                                                    onClick={() => this.handleImageUrl()}
                                                    component="span"
                                                    size="large"
                                                    variant="contained"
                                                    color="primary"
                                                >
                                                    Загрузить
                                                </Button>
                                            </Grid>

                                        </Grid>

                                    </Grid>
                                </Modal>
                            </Grid>

                            <Grid item>
                                <input
                                    accept="image/*"
                                    className={classes.input}
                                    id={`upload-image-button-for-${field}`}
                                    type="file"
                                    onChange={this.handleImage}
                                    multiple={isMulti}
                                />
                                <label htmlFor={`upload-image-button-for-${field}`}>
                                    <Button
                                        component="span"
                                        size="large"
                                        variant="contained"
                                        color="primary"
                                    >
                                        Загрузить изображение
                                    </Button>
                                </label>
                            </Grid>

                        </Grid>

                    </FormControl>
                </H2Flex>
                <SortabelPaperPadded classes={classes} isMulti={isMulti} images={this.state.previews}
                                     onSortEnd={this.onSortEnd} onSortStart={this.onSortStart}
                                     onDelete={this.deleteUploadedImage} error={error}
                                     axis='xy' showDeleteIcon={this.state.showDeleteIcon}/>
            </React.Fragment>
        );
    }
}

export default withStyles(theme => ({
    input: {
        display: 'none',
    },
    paper: {
        position: 'relative',
        display: 'block',
        alignItems: 'right',
        cursor: "pointer",
    },
    message: {
        marginLeft: theme.spacing.unit * 2,
    },
    paperContent: {
        position: 'relative',
        width: 100,
        marginRight: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        float: 'left',
        height: '130px',

    },
    icon: {
        position: 'absolute',
        right: -theme.spacing.unit,
        top: -theme.spacing.unit,
        zIndex: 2,
        background: 'white',
        borderRadius: '50%',
    },
    clear: {
        clear: 'both'
    },
    modal: {
        position: 'relative',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        top: '50%',
        left: '50%',
        transform: `translate(-50%, -50%)`,

    },
    modalItem: {
        justifyContent: 'center',
    }
}))(UploadImage);
