import React from 'react';
import { withStyles } from "@material-ui/core";
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
const styles = theme => ({
    close: {
        width: theme.spacing.unit * 4,
        height: theme.spacing.unit * 4,
    },
});

import {
    eventManager,
    ACTION_SHOW_NOTIFICATION
} from "../../core/eventManager";

class Notification extends React.Component<{

}, {
    open: boolean,
    title: string
}>{
    state = {
        open: false,
        title: ''
    };

    constructor(props){
        super(props);
    };

    componentDidMount(){
        eventManager.on(ACTION_SHOW_NOTIFICATION, content => this.show(content))
    }

    componentWillUnmount(){
        eventManager.off(ACTION_SHOW_NOTIFICATION);
    }

    handleClose = () => {
        this.setState({
            open: false
        });
    };

    show(content) {
        this.setState({open: true, title: content});
    }

    render(){
        return(
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={this.state.open}
                autoHideDuration={3000}
                onClose={this.handleClose}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{this.state.title}</span>}
                action={[
                    <Button key="undo" color="secondary" size="small" onClick={this.handleClose}>
                        UNDO
                    </Button>,
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        onClick={this.handleClose}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />
        )
    }
}

export default withStyles(styles)(Notification);