import {
    eventManager,
    ACTION_SHOW_NOTIFICATION
} from '../../core/eventManager';

function notificate(content) {
    eventManager.emit(ACTION_SHOW_NOTIFICATION, content);
}

export {notificate};