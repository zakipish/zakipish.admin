import * as React from "react";
import { Link } from "react-router-dom";
import { LocationDescriptor } from 'history';
import Button, { ButtonProps } from '@material-ui/core/Button';
import IconButton, { IconButtonProps } from '@material-ui/core/IconButton';
import BackIcon from "@material-ui/icons/ArrowBackIos"
import SaveIcon from "@material-ui/icons/SaveOutlined";
import AddIcon from '@material-ui/icons/Add';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';


interface ButtonLinkProps extends ButtonProps {
  // ListItemProps and LinkProps both define an 'innerRef' property
  // which are incompatible. Therefore the props `to` and `replace` are
  // simply duplicated here.
  to: LocationDescriptor
  replace?: boolean
}

export class ButtonLink extends React.PureComponent<ButtonLinkProps> {
  render() {
      return <Button {...this.props} component={Link}/>
  }
}

interface IconButtonLinkProps extends IconButtonProps {
  to: LocationDescriptor
  replace?: boolean
}

export class IconButtonLink extends React.PureComponent<IconButtonLinkProps> {
  render() {
      return <IconButton {...this.props} component={Link}/>
  }
}

export const BackButton: React.StatelessComponent<{
  to: string
}> = ({to}) => {
  return (
    <ButtonLink 
      mini  
      style={{padding: 0,
        minWidth: '2em',
        marginTop: '-0.5em',
      }} 
      to={to}
    >
      <BackIcon 
        style={{
          fontSize: '3.2em',
        }} 
        color="primary" 
      />
    </ButtonLink>
  );
};


const SaveButtonComponent: React.StatelessComponent<{
  classes: any,
  onClick: () => void,
  loading: boolean
  disabled: boolean
}> = ({onClick, classes, loading, disabled}) => {
  return (
    <React.Fragment>
      <Button 
        type="submit"
        variant="extendedFab"
        color="primary" 
        size="large"
        onClick={onClick}
        disabled={loading || disabled}
      >
        <SaveIcon className={classes.leftIcon} /> 
        Сохранить
      </Button>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} color="secondary"/>}
    </React.Fragment>
  );
};
export const SaveButton = withStyles(theme => ({
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  buttonProgress: {
    // color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}))(SaveButtonComponent);



const AddButtonComponent: React.StatelessComponent<{
  classes: any,
  onClick?: () => void,
  to?: string,
  busy?: boolean
}> = ({onClick, classes, busy, to}) => {
  return (
    <div className={classes.root}>
      {onClick ? (
        <Button 
          variant="fab" 
          color="primary" 
          aria-label="Add" 
          className={classes.fab}
          onClick={onClick}
          disabled={busy}
        >
          <AddIcon />
        </Button>
      ) : to ? (
        <ButtonLink
          variant="fab" 
          color="primary" 
          aria-label="Add" 
          className={classes.fab}
          to={to}
          disabled={busy}
        >
          <AddIcon />
        </ButtonLink>
      ) : ''}
    </div>
  );
};
export const AddButton = withStyles(theme => ({
  root: {
    marginBottom: theme.spacing.unit * 20,
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
}))(AddButtonComponent);