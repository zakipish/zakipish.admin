import React from "react";

import { withStyles } from '@material-ui/core/styles';
import TableCell, {TableCellProps} from '@material-ui/core/TableCell';

export const ThinTableCell = withStyles(theme => ({
  head: {
    width: '1%',
  },
  body: {
    width: '1%',
  },
}))(TableCell);

interface ExtendedTableCellInterface {
  thin?: boolean;
  disabled?: boolean;
  minWidth?: number;
}

export const ExtendedTableCell: React.StatelessComponent<TableCellProps & ExtendedTableCellInterface> = ({disabled, thin, minWidth, ...props}) => {
  const ExtendedTableCellElement = withStyles(theme => ({
    head: {
      width: thin ? '1%' : 'auto',
      minWidth: minWidth || 'auto' ,
    },
    body: {
      minWidth: minWidth || 'auto' ,
      width: thin ? '1%' : 'auto',
      opacity: disabled ? .3 : 1,
    },
  }),{withTheme: true})(TableCell);
  return (
    <ExtendedTableCellElement {...props}/>
  );
}