import * as React from "react";

import Fade from '@material-ui/core/Fade';
import Collapse from '@material-ui/core/Collapse';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from "@material-ui/core";
import { classExpression } from "babel-types";
import TableBody, { TableBodyProps } from "@material-ui/core/TableBody";

const Loader:React.StatelessComponent<{
  loading: boolean,
  classes: any,
  hideCircle?: boolean,
}> = ({loading, children, classes, hideCircle}) => {

  return (
    <React.Fragment>
      <Fade
        in={!hideCircle && loading}
        style={{
          position: 'absolute',
        }}
        unmountOnExit
      >
        <CircularProgress />
      </Fade>
      <Collapse in={!loading} className={classes.collapse} classes={{
        entered: classes.entered,
      }}>
        <div className={classes.wrapper}>
          {children}
        </div>
      </Collapse>
    </React.Fragment>
  );
};

export default withStyles(theme => ({
  collapse: {
    margin: -theme.spacing.unit,
  },
  wrapper: {
    padding: theme.spacing.unit,
  },
  entered: {
    overflow: 'visible',
  }
}))(Loader);


interface TableBodyLoaderProps extends TableBodyProps {
    loading: boolean;
}

export const TableBodyLoader:React.ComponentType<TableBodyLoaderProps> = ({loading, children, ...rest}) => {
  return (
    <TableBody {...rest} style={{
      position: 'relative',
      opacity: loading ? .2 : 1,
    }}>
      {children}
      {/* {loading && <CircularProgress size={24} style={{
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
      }} color="secondary"/>} */}
    </TableBody>
  );
};

interface ShadowLoaderProps extends TableBodyProps {
  loading: boolean;
}

export const ShadowLoader:React.ComponentType<ShadowLoaderProps> = ({loading, children}) => {
  return (
    <div style={{
      position: 'relative'
    }}>
      {children}
      {loading && (
        <div style={{
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          background: 'rgba(0,0,0,.5)'
        }}>
          <CircularProgress size={24} style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: -12,
            marginLeft: -12,
          }} color="secondary"/>
        </div>
      )}
    </div>
  );
};