import * as React from "react";

import { withStyles } from '@material-ui/core/styles';
import { Paper } from "@material-ui/core";

export const PaperPadded = withStyles(theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
  }))(Paper);
