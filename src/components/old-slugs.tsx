import * as React from "react";
import { ListItem, ListItemText, List } from "@material-ui/core";

import { H2 } from "./titles";

const OldSlugs:React.StatelessComponent<{
  slugs: [{
    value: string,
    is_active: boolean,
  }],
}> = ({slugs}) => {
const oldSlugs = slugs.filter(item => !item.is_active);
return oldSlugs.length ? (
  <React.Fragment>
    <H2>Старые ЧПУ</H2>
    <List>
      {oldSlugs.map(item => 
        <ListItem key={item.value}>
          <ListItemText primary={item.value} />
        </ListItem>
      )}
     </List>
  </React.Fragment>
) : null;
};

export default OldSlugs;