import * as React from "react";

import TablePagination from '@material-ui/core/TablePagination';
import { CircularProgress } from "@material-ui/core";

import { apiGet } from "../core/api";
import { history } from "../core/history";


const Loader:React.ComponentType<{
  loading: boolean;
}> = ({ loading, children }) => {
  return (
    <div style={{
      position: 'relative',
      opacity: loading ? .2 : 1,
    }}>
      {children}
      {loading && <CircularProgress size={48} style={{
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -24,
        marginLeft: -24,
      }} color="secondary"/>}
    </div>
  );
};

export default class Pagination extends React.Component<{
  onRef?:any,
  url: string,
  children: (items: any[]) => JSX.Element,
}>
{
  mounted = false;
  unmounted = false;

  state = {
    items: [],
    rowsPerPage: parseInt(localStorage.getItem('defaul_rows_per_page'))?parseInt(localStorage.getItem('defaul_rows_per_page')):50,
    page: 0,
    loading: true,
    pagination: {
      total: 0,
      per_page: 50,
      current_page: 0
    },
  }

  constructor(props) {
    super(props);

  }
  componentDidMount() {
    //@ts-ignore
    if (this.props.onRef)
      this.props.onRef(this);
    this.bootstrap();
    this.mounted = true;
  }

  componentWillUnmount() {
    this.unmounted = true;
    //@ts-ignore
    if (this.props.onRef)
      this.props.onRef(undefined);

  }

  setStateObj = (newState) => {
    if (this.unmounted) {
      return;
    }
    if (this.mounted) {
      this.setState(newState);
    } else {
      this.state = {...this.state, ...newState};
    }
  };

  bootstrap = async (filters=null) => {
    let { rowsPerPage, page } = this.state;
    const { url } = this.props;
    const query_params = new URLSearchParams(window.location.search);
    let rank = query_params.has('rank')? query_params.get('rank'):0;
    let club = query_params.has('club_chain_id')? query_params.get('club_chain_id'):0;
    let city = query_params.has('city_id')? query_params.get('city_id'):0;
    let sport = query_params.has('sport_id')? query_params.get('sport_id'):0;
    let name = query_params.has('name')? query_params.get('name'):0;
    let source = query_params.has('source')? query_params.get('source'):0;
    let sorted_field = query_params.has('sorted_field')? query_params.get('sorted_field'):0;
    let sorted_order = query_params.has('sorted_order')? query_params.get('sorted_order'):0;
    if (filters){

      rank = filters.rank;
      club = filters.club_chain_id;
      city = filters.city_id;
      name = filters.name;
      source = filters.source;
      sport = filters.sport_id;
      sorted_field = filters.sorted_field;
      sorted_order = filters.sorted_order;
      page = 0;
    }
    let query = [];
    if (!this.mounted){
      if (query_params.has('page')){
        page= parseInt(query_params.get('page')) - 1;
      }
      if (query_params.has('perPage')){
        rowsPerPage = parseInt(query_params.get('perPage'))
      }
      if (query_params.has('city_id')){

      }
    }
    if (page > 0) {
      query.push(`page=${page + 1}`);
    }
    if (rowsPerPage !== 50) {
      query.push(`perPage=${rowsPerPage}`);
    }
    if (city) {
      query.push(`city_id=${city}`)
    }
    if (rank) {
      query.push(`rank=${rank}`)
    }
    if (club) {
      query.push(`club_chain_id=${club}`)
    }
    if (name){
      query.push(`name=${name}`)
    }
    if (source){
      query.push(`source=${source}`)
    }
    if (sorted_field){
      query.push(`sorted_field=${sorted_field}`)
    }
    if (sorted_order){
      query.push(`sorted_order=${sorted_order}`)
    }
    if (sport){
      query.push(`sport_id=${sport}`)
    }
    history.push(url + query.length ? '?' + query.join('&') : '');

    this.setStateObj({
      loading: true,
    });
    let params = {
      page: page + 1,
      per_page: rowsPerPage,
    };

    if (sport)
      params['sport_id'] = sport;
    if (city)
      params['city_id'] = city;
    if (rank)
      params['rank'] = rank;
    if (club)
      params['club_chain_id'] = club;
    if (name)
      params['name'] = name;
    if (source)
      params['source'] = source;
    if (sorted_field)
      params['sorted_field'] = sorted_field;
    if (sorted_order)
      params['sorted_order'] = sorted_order;
    const data = await apiGet(url, params);

    rowsPerPage === data.meta.per_page
    && (page + 1) === data.meta.current_page
    && this.setStateObj({
      items: data.data,
      pagination: data.meta,
      loading: false,
      paginate: false,
      page:page,
      rowsPerPage:rowsPerPage
    })
  };

  handleChangePage = (event, page) => {
    this.setState({ page }, this.bootstrap);
  };

  handleChangeRowsPerPage = event => {
    this.setState({
      rowsPerPage: event.target.value,
      page: 0,
    }, this.bootstrap);
    localStorage.setItem('defaul_rows_per_page', event.target.value);
  };

  render() {
    const { loading, items, rowsPerPage, page, pagination } = this.state;
    const { children } = this.props;

    if (!items) {
      return (
        <CircularProgress />
      );
    }
    return (
      <React.Fragment>
        <Loader loading={loading}>
          { children(items) }
        </Loader>
        <TablePagination
          component="div"
          count={pagination.total}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[50, 75, 100]}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Назад',
          }}
          nextIconButtonProps={{
            'aria-label': 'Вперед',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </React.Fragment>
    );
  }
};
