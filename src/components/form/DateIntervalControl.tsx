import React from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import {FormControl, FormHelperText, FormLabel, StandardProps} from "@material-ui/core";
import moment from "moment";
import { FormControlProps, FormControlClassKey } from '@material-ui/core/FormControl';
import DateControl from './DateControl';


interface DateIntervalControlProps 
  extends StandardProps<FormControlProps, FormControlClassKey, 'onChange'> {
  type: 'date' | 'datetime' | 'datetime-local' | 'time',
  //текущее значение
  value: [string, string],
  //метод для изменения стейта из родителя
  onChange: (value: [string, string]) => void,
  validation?: any,
  label?: string,
  errorText?: string,
  helperText?: string,
}

export interface DateIntervalControlState {
  validateInfo: any;
}


export default class DateIntervalControl extends React.Component<DateIntervalControlProps, DateIntervalControlState> {

  constructor(props) {
    super(props);
    this.state = {
      validateInfo: [{flag: false, message: ''}],
    }
  }
  
  get selfFormat() {
    return {
      'date': moment.HTML5_FMT.DATE,
      'datetime': moment.HTML5_FMT.DATETIME_LOCAL,
      'datetime-local': moment.HTML5_FMT.DATETIME_LOCAL,
      'time': moment.HTML5_FMT.TIME,
    }[this.props.type];
  };

  //validate date-interval
  validateDate = (value: [string, string]) => {
    if (value) {
      let firstDate = moment(value[0], this.selfFormat);
      let lastDate = moment(value[1], this.selfFormat);
      let result = firstDate.isAfter(lastDate);

      //validate result to parent
      if(this.props.validation){
        this.props.validation(result);
      }

      return ({flag: result, message: 'Дата начала должна быть раньше даты конца'});
    }
    return ({flag: false, message: ''});
  };

  //first validate
  componentDidMount() {
   this.setState({validateInfo: this.validateDate(this.props.value)});
  }

  //validate when update
  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setState({validateInfo: this.validateDate(this.props.value)});
    }
  }

  render() {
    const { type, value, onChange, label, errorText, helperText, validation, ...props } = this.props;
    return (
      <FormControl error={ !!(errorText || this.state.validateInfo['flag']) } {...props}>
        <FormLabel component="legend">{label}</FormLabel>
        <Grid container spacing={16}>
          <Grid item xs={12} sm>
            <DateControl
              margin="normal"
              fullWidth
              label={(type === 'time') ? "Время начала" : 'Дата начала'}
              type={type}
              value={value ? value[0] : ''}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(leftValue) => onChange([leftValue, value ? value[1] : ''])}
            />

          </Grid>
          <Grid item xs={12} sm>
            <DateControl
              margin="normal"
              fullWidth
              label={(type === 'time') ? "Время окончания" : 'Дата окончания'}
              type={type}
              value={value ? value[1] : ''}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(rightValue) => onChange([value ? value[0] : '', rightValue])}
            />
          </Grid>
        </Grid>
        <FormHelperText>
          { errorText || (this.state.validateInfo['flag'] && this.state.validateInfo['message']) || helperText }
        </FormHelperText>
      </FormControl>
    );
  }

}