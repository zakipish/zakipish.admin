import React from 'react';
import { ExpansionPanel, ExpansionPanelSummary, withStyles, ExpansionPanelDetails } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { H3 } from '../titles';

const styles = theme => ({
  expansion: {
    marginTop: theme.spacing.unit*2,
  },
  expansionExpanded: {
    overflow: 'visible',
  },
});

class Expansion extends React.PureComponent<{
  title: string,
  classes: any,
}> {
  render() {
    const { title, classes, children, ...props } = this.props;
      return (
        <ExpansionPanel 
          classes={{ 
            root: classes.expansion,
            expanded: classes.expansionExpanded 
          }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <H3>{ title }</H3>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails
            style={{
              overflow: 'visible'
            }}
            classes={{ 
              root: classes.expansionExpanded 
            }}
            className={classes.expansionExpanded}
          >
              { children }
          </ExpansionPanelDetails>
        </ExpansionPanel>
      );
  }
}

export default withStyles(styles)(Expansion);