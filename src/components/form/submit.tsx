import * as React from "react";
import { withStyles, Typography, FormControl } from "@material-ui/core";
import { SaveButton } from "../buttons";
import Error from "./error";


export default withStyles(theme => {
  console.log(theme);
  return {
  float: {
    position:"fixed",
    bottom: "10px",
    right: "10px",
    zIndex: theme.zIndex.appBar,
  },
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
  }, 
  message: {
    marginRight: theme.spacing.unit * 2,
  }
}
})(
class Submit extends React.Component<{
  classes: any,
  float?: boolean,
  disabled: boolean,
  loading: boolean,
  message?: string,
  onSubmit: () => void,
}>
{
  render() {
    const { classes, float, disabled, loading, message, onSubmit } = this.props;
    return (
      <div className={`${classes.root} ${float ? classes.float : ''}`}>
        <Error className={classes.message} message={message} />
        <FormControl>
          <SaveButton onClick={onSubmit} loading={loading} disabled={disabled} />
        </FormControl>
      </div>
    );
  }
});
