import * as React from "react";

import { TextField } from "@material-ui/core";
import { TextFieldProps } from "@material-ui/core/TextField";


interface TextControlProps extends TextFieldProps {
    change: (value: string) => void,
    errorText?: string,
}

class InputControl extends React.Component<TextControlProps> {
    render() {
        const { value, change, errorText, helperText, ...rest } = this.props;
        return (
            <TextField
                value={value || ''}
                onChange={(e) => change(e.target.value)}
                margin="normal"
                error={ !!errorText }
                helperText={ errorText || helperText }
                {...rest}
            />
        );
    }
};

export default InputControl;