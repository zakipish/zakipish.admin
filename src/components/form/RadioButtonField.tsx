import React from 'react';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl, { FormControlProps } from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

type RadioItem = {
  label:string,
  value:string,
  disabled?: boolean,
}
type RadioButtonFieldValue = number|string
export default class RadioButtonField extends React.Component<FormControlProps & {
  name: string,
  label?: string,
  fields: RadioItem[],
  value: string,
  onChange: (value: string) => void,
  errorText?: string,
  helperText?: string,
},{value: string }>{
  render() {
    const {value, label, name, fields, onChange, errorText, helperText, ...props} = this.props;
    return(
      <FormControl error={ !!errorText } {...props}>
        <FormLabel component="legend">{label}</FormLabel>
        <RadioGroup
          aria-label={name}
          name={name}
          value={value}
          onChange={(e, value) => onChange(value)}
        >
          {fields.map((item, i)=> (
            <FormControlLabel
              key={i}
              value={item.value}
              disabled={item.disabled}
              control={<Radio />}
              label={item.label}
            />
          ))}
        </RadioGroup>
        <FormHelperText>
          { errorText || helperText }
        </FormHelperText>
      </FormControl>
    );
  }

}