import React from 'react';
import SelectControl from './select-control';
import { FormControlProps, FormControlClassKey } from '@material-ui/core/FormControl';
import { StandardProps } from '@material-ui/core';

interface SelectWeekControlProps 
  extends StandardProps<FormControlProps, FormControlClassKey, 'onChange'> {
  value: number,
  onChange: (value: number) => void,
  label?: string,
  errorText?: string,
  helperText?: string,
}

export default class SelectWeekControl extends React.Component<SelectWeekControlProps>
{

  weekdays = [
    {
      label: 'Понедельник',
      value: 1,
    },
    {
      label: 'Вторник',
      value: 2,
    },
    {
      label: 'Среда',
      value: 3,
    },
    {
      label: 'Четверг',
      value: 4,
    },
    {
      label: 'Пятница',
      value: 5,
    },
    {
      label: 'Суббота',
      value: 6,
    },
    {
      label: 'Понедельник',
      value: 0,
    },
  ];

  render () {
    const { value, onChange, errorText, helperText, ...rest } = this.props;
    return (
      <SelectControl
          {...rest}
          suggestions={this.weekdays}
          value={value}
          onChange={onChange}
          error={!!errorText}
          errorText={ errorText || helperText }
      />
    );
  }
}

