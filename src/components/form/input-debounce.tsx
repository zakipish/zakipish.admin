import * as React from "react";

import { TextField } from "@material-ui/core";
import { TextFieldProps } from "@material-ui/core/TextField";
import Input from "../../components/form/input-control";
import {debounce} from 'lodash'

interface TextControlProps extends TextFieldProps {
    change: (value: string) => void,
    errorText?: string,
  debounceLat:number
}

class InputControl extends React.Component<TextControlProps> {
    state={
        name:""
    };
    componentDidMount(){
      this.setState({
        name:this.props.value
      })
    }
    nameChandge(value){
        this.setState({name:value});
        this.debounceName(value);
    }
    debounceName=debounce((value)=>{
        const {change} = this.props;
        change(value);},this.props.debounceLat);
    render() {
        const { value, change, errorText, label, helperText, ...rest } = this.props;
        return (
            <Input
                value={this.state.name}
                change={(value)=>{this.nameChandge(value)}}
                label={label}
                margin="normal"
                fullWidth
            />
        );
    }
};

export default InputControl;