import React from 'react';
import moment from "moment";
import TextField, { TextFieldProps, TextFieldClassKey } from "@material-ui/core/TextField/TextField";
import { StandardProps } from "@material-ui/core";


interface DateControlProps
  extends StandardProps<TextFieldProps, TextFieldClassKey, 'onChange' | 'type'> {
  type: 'date' | 'datetime' | 'datetime-local' | 'time',
  // текущее значение
  value: string,
  // метод для изменения стейта из родителя
  onChange: (value: string) => void,
  // ошибки
  errorText?: string
}

export default class DateControl extends React.Component<DateControlProps> {

  get dbFormat() {
    return {
      'date': 'YYYY-MM-DD',
      'datetime': 'YYYY-MM-DDTHH:mm:ss',
      'datetime-local': 'YYYY-MM-DDTHH:mm:ss',
      'time': 'HH:mm',
    }[this.props.type];
  };

  get selfFormat() {
    return {
      'date': moment.HTML5_FMT.DATE,
      'datetime': moment.HTML5_FMT.DATETIME_LOCAL,
      'datetime-local': moment.HTML5_FMT.DATETIME_LOCAL,
      'time': moment.HTML5_FMT.TIME,
    }[this.props.type];
  };

  //convert from datapicker to db format and emit changes
  handleChange = (date) => {
    if (date && moment(date, this.selfFormat).isValid()) {
      let result = moment(date, this.selfFormat).format(this.dbFormat);
      console.log('handleChange', date, result);
      this.props.onChange(result);
    }
  };

  //convert from db format to datapicker
  convertToDataPickerFormat = (date) => {
    let result = '';
    if (date && moment(date, this.dbFormat).isValid()) {
      result = moment(date, this.dbFormat).format(this.selfFormat)
    }
    return result;
  };

  render() {
    const { type, value, onChange, errorText, helperText, ...rest } = this.props;

    return (
      <TextField
        {...rest}
        type={type}
        value={this.convertToDataPickerFormat(value)}
        onChange={(e) => this.handleChange(e.target.value)}
        error={ !!errorText }
        helperText={ errorText || helperText }
        InputLabelProps={{
          shrink: true,
        }}
      />
    );
  }

}