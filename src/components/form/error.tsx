import React from 'react';
import { Typography } from '@material-ui/core';
import { TypographyProps } from '@material-ui/core/Typography';

export default class Error extends React.PureComponent<TypographyProps & {
  message: string,
}> {
  render() {
    const { message, ...props } = this.props;
      return (
        <Typography 
          color="error"
          variant="body1"
          {...props}
        >
          { message }
        </Typography>
      );
  }
}