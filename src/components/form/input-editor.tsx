import * as React from "react";

import { TextField } from "@material-ui/core";
import { TextFieldProps } from "@material-ui/core/TextField";
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
interface TextControlProps extends TextFieldProps {
    change: (value: string) => void,
    errorText?: string,
}
const customToolbar = {
    options: ['inline', 'list','blockType', 'fontSize', 'remove'],
    inline: {
        inDropdown: false,
        className: undefined,
        component: undefined,
        dropdownClassName: undefined,
        options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace'],
    },
    blockType: {
        inDropdown: true,
        options: ['Normal', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'Blockquote', 'Code'],
        className: undefined,
        component: undefined,
        dropdownClassName: undefined,
    },

};
class InputEditor extends React.Component<TextControlProps,{editorState:any}> {
    constructor(props:any){
        super(props);
        this.state = {editorState : EditorState.createEmpty()};
    }
    componentDidMount(){
        let {value} = this.props;
        if (!value){
            value = '';
        }
        const content = htmlToDraft(value);
        const { contentBlocks, entityMap } = content;
        const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
        this.setState({
            editorState: EditorState.createWithContent(contentState)
        });
    }
    chandgeText(editorState){
        const {change} = this.props;
        this.setState({editorState:editorState});
        let HTMLContent = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        change(HTMLContent);
    }
    onPaste(text:any){
      console.log('aaa')
    }
    render() {
        const { value, label, change, errorText, helperText, ...rest } = this.props;
        const { editorState } = this.state;
        // @ts-ignore
      return (
            <div style={{margin:"16px 0 8px 0"}}>
                <label htmlFor="" style={{color: "rgba(0, 0, 0, 0.54)", fontSize:"0.8571428571428571rem", fontFamily:"Open Sans, Helvetica, Arial, serif" }} >{label}</label>
                <div style={{padding:"5px",marginTop:"5px"}}>
                <Editor
                editorState={editorState}
                toolbar = {customToolbar}
                onEditorStateChange={(editorState)=>{this.chandgeText(editorState)}}
                handlePastedText={()=>false}
                stripPastedStyles={true}
                />
                </div>
            </div>
        );
    }
};

export default InputEditor;
