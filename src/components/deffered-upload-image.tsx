import * as React from "react";
import {FormControl, Button, Card, CircularProgress, Grid, Modal, TextField} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles';
import {H2Flex} from "./titles";
import {PaperPadded} from "./paper";
import {apiPost} from "../core/api";
import {P} from "./p";
import Error from "./form/error";
import {AssignableObject} from "../core/utils";
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import ClearIcon from '@material-ui/icons/Clear';

interface UploadedImage {
    id: number;
    order: number;
    url: string
}

class UploadingImage extends AssignableObject {
    id: number;
    order: number;
    url: string;
    file: File;
}

interface ImageBlockProps {
    classes: any;
    image: UploadingImage | UploadedImage;
    index: number;
    loading?: boolean;
}

const ImageBlockComponent: React.StatelessComponent<ImageBlockProps> = ({classes, image, loading}) => (
        <Card className={classes.preview} style={{zIndex: 1}}>
            <img className={classes.img} src={image.url} />
            {loading && (
                <div className={classes.progressWrapper}>
                    <CircularProgress size={24} className={classes.buttonProgress} color="secondary"/>
                </div>
            )}
        </Card>
    ),

    ImageBlock = withStyles(theme => ({
        preview: {
            maxWidth: 100,
            maxHeight: 100,
            position: 'relative',
            zIndex: 1
        },
        buttonProgress: {
            // color: green[500],
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: -12,
            marginLeft: -12,
        },
        progressWrapper: {
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: "rgba(255,255,255,0.5)",
        },
        img: {
            display: 'block',
            width: '100%',
        },
    }))(ImageBlockComponent);

//Sortable adding
const SortableImageBlock = SortableElement(ImageBlock);

interface ImagesContainerProps {
    classes: any;
    images: any;
    uploadingImages: any;
    error: any;
    onDelete: any;
    axis: any;
    showDeleteIcon: boolean;
}

const ImagesContainer: React.StatelessComponent<ImagesContainerProps> = ({classes, images, uploadingImages, error, onDelete, axis, showDeleteIcon}) => (
    <div className={classes.paper}>
        {images.map((image, i) => (
            <div className={classes.paperContent} key={i}>
                {showDeleteIcon && (
                    <ClearIcon
                        className={classes.icon}
                        color="secondary"
                        fontSize="small"
                        onClick={onDelete}
                    />
                )}

                <SortableImageBlock key={i} index={i} image={image} />
            </div>
        ))}

        {uploadingImages.map((image, i) => (
            <div className={classes.paperContent} key={i}>
                <SortableImageBlock key={i} index={i} image={image} loading />
            </div>
        ))}

        {images.length && images.length === 0 && <P align="center">Изображений нет</P>}
        {error && <Error className={classes.message} message={error}/>}

        <div className={classes.clear}/>
    </div>
);
const SortableImagesContainer = SortableContainer(ImagesContainer);

interface Props {
    onChange: (images: any[]) => void;
    classes: any;
    url: string;
    field: string;
    title: string;
    images?: UploadedImage[];
}

interface State {
    uploadingImages: UploadingImage[];
    error: string;
    showDeleteIcon: boolean;
    showUrlModal: boolean;
    imageUrl: string;
}

class DefferedUploadImage extends React.Component<Props, State> {
    state: State = {
        uploadingImages: [],
        error: null,
        showDeleteIcon: true,
        showUrlModal: false,
        imageUrl: null,
    };

    createImageFromFile = (file: File) => {
        return new UploadingImage({
            file: file,
            url: URL.createObjectURL(file),
        });
    }

    uploadImages = async (form) => {
        const { url, images } = this.props;
        let oldImages = images.slice();

        try {
            const result = await apiPost(url, form)
            const sortedUploadedImages = images.sort((a, b) => a.order - b.order)
            let order = (sortedUploadedImages.length > 0 ? sortedUploadedImages[0].order : 0) + 1;

            const newImages = result.data.map((image) => {
                return {
                    id: image.id,
                    order: order++,
                    url: image.url,
                };
            });

            oldImages = oldImages.concat(newImages);
            return oldImages;
        } catch (e) {
            this.setState({
                error: e.message || 'Some error',
            })
        }
    }

    makeForm = (field, images) => {
        let form = new FormData();
        images.forEach(image => {
            form.append(field, image.file, image.file.name);
        });

        return form
    }

    handleImage = async (e) => {
        const { field, onChange } = this.props;
        const { files } = e.target;

        if (!files || !files.length) return;

        const uploadingImages = []

        for (let i = 0; i < files.length; i++) {
            uploadingImages.push(this.createImageFromFile(files[i]));
        }

        this.setState({
            uploadingImages,
            error: null,
        }, () => {
            const form = this.makeForm(field, uploadingImages)
            this.uploadImages(form)
            .then((result) => {
                onChange(result)
                this.setState({
                    uploadingImages: []
                });
            });
        });
    };

    deleteImage = (i: number) => {
        const { images, onChange } = this.props;
        const sortedImages = images.sort((a, b) => b.order - a.order);
        onChange(sortedImages.splice(i, 1))
    };

    onSortStart = () => {
        this.setState({ showDeleteIcon: false });
    };

    onSortEnd = ({ oldIndex, newIndex }) => {
        const { images, onChange } = this.props
        const newSortedImages = arrayMove(images, oldIndex, newIndex)
        const newImages = newSortedImages.map(( image, index ) => ({
            ...image,
            order: index
        }))

        onChange(newImages)
        this.setState({ showDeleteIcon: true });
    };

    /*
    //show modal for images form irl
    handleUrlModalOpen = () => {
        this.setState({showUrlModal: true});
    };

    //close modal for images form irl
    handleUrlModalClose = () => {
        this.setState({showUrlModal: false});
    };

    //check URL link
    changeUrl = (e) => {
        this.setState({imageUrl: e.target.value});
    }
     */

    /*
    //here link go to the back-end
    handleImageUrl = async () => {
        const { field } = this.props;
        let { uploadingImages } = this.state;
        let form = new FormData();

        form.append('_method', 'PATCH');
        form.append('imageUrl', this.state.imageUrl);
        form.append('field', field);
        let newPreview;
        try {
            let promise = await apiPost(this.props.url, form);
            newPreview = promise
        } catch (e) {
            this.setState({
                error: e.message || 'Some error',
            })
        }
        this.handleUrlModalClose();
        let image = new Image({
            id: newPreview[0].id,
            order: newPreview[0].order,
            preview: newPreview[0].preview,
        });
        if (field === 'logo') {
            this.setState({
                uploadingImages: [image],
            });
        } else {
            this.setState({
                previews: [...previews, image],
            });
        }
    }
    */

    render() {
        const { classes, images, title, field } = this.props;
        const { error, uploadingImages } = this.state;
        return (
            <React.Fragment>
                <H2Flex>
                    <FormControl margin="normal">
                        <span>{title}</span>
                    </FormControl>
                    <FormControl margin="normal">

                        <Grid container spacing={8}>
                            {/*
                            <Grid item>
                                <Button
                                    component="span"
                                    size="large"
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleUrlModalOpen}
                                >
                                    Загрузить из URL
                                </Button>
                                <Modal
                                    aria-labelledby="simple-modal-title"
                                    aria-describedby="simple-modal-description"
                                    open={this.state.showUrlModal}
                                    onClose={this.handleUrlModalClose}
                                >

                                    <Grid className={classes.modal}>
                                        <Grid container className={classes.modalItem} style={{width: "100%"}}
                                              spacing={8}>
                                            <Grid item>
                                                <TextField
                                                    id={`imageUrl-${field}`}
                                                    label="URL"
                                                    onChange={this.changeUrl}
                                                >
                                                </TextField>
                                            </Grid>
                                        </Grid>
                                        <Grid container className={classes.modalItem} spacing={8}>
                                            <Grid item>
                                                <Button
                                                    onClick={() => this.handleImageUrl()}
                                                    component="span"
                                                    size="large"
                                                    variant="contained"
                                                    color="primary"
                                                >
                                                    Загрузить
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Modal>
                            </Grid>
                            */}

                            <Grid item>
                                <input
                                    accept="image/*"
                                    className={classes.input}
                                    id={`upload-image-button-for-${field}`}
                                    type="file"
                                    onChange={this.handleImage}
                                    multiple
                                />
                                <label htmlFor={`upload-image-button-for-${field}`}>
                                    <Button
                                        component="span"
                                        size="large"
                                        variant="contained"
                                        color="primary"
                                    >
                                        Загрузить изображение
                                    </Button>
                                </label>
                            </Grid>

                        </Grid>

                    </FormControl>
                </H2Flex>

                <SortableImagesContainer
                    classes={classes}
                    images={images}
                    uploadingImages={uploadingImages}
                    onSortEnd={this.onSortEnd} onSortStart={this.onSortStart}
                    onDelete={this.deleteImage}
                    error={error}
                    axis="xy"
                    showDeleteIcon={this.state.showDeleteIcon}
                />
            </React.Fragment>
        );
    }
}

export default withStyles(theme => ({
    input: {
        display: 'none',
    },
    paper: {
        position: 'relative',
        display: 'block',
        alignItems: 'right',
        cursor: "pointer",
    },
    message: {
        marginLeft: theme.spacing.unit * 2,
    },
    paperContent: {
        position: 'relative',
        width: 100,
        marginRight: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        float: 'left',
        height: '130px',

    },
    icon: {
        position: 'absolute',
        right: -theme.spacing.unit,
        top: -theme.spacing.unit,
        zIndex: 2,
        background: 'white',
        borderRadius: '50%',
    },
    clear: {
        clear: 'both'
    },
    modal: {
        position: 'relative',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        top: '50%',
        left: '50%',
        transform: `translate(-50%, -50%)`,

    },
    modalItem: {
        justifyContent: 'center',
    }
}))(DefferedUploadImage);
