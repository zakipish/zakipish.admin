import { AssignableObject } from '../core/utils';

export class Image extends AssignableObject {
    id: number
    url: string
    default?: string
    order?: number
}
