import { AssignableObject } from "../core/utils";

export class ClubChain extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}