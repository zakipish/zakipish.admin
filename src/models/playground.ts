import { AssignableObject } from "../core/utils";

export class Playground extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}