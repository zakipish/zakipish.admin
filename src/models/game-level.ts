import { AssignableObject } from "../core/utils";

export class GameLevel extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}
