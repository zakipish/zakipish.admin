import { AssignableObject } from "../core/utils";

export class City extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}