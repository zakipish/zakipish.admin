import { AssignableObject } from "../core/utils";
import { User } from "./user";

export class Game extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
  description: string;
  users: User[] = [];
}
