import { AssignableObject } from "../core/utils";

export class PlaygroundType extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}