import { AssignableObject } from "../core/utils";
import { ParticipationType } from './event'

export class User extends AssignableObject {
    id: Number;
    name: string;
    email: string;
    type: ParticipationType;
}
