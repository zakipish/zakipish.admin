import { AssignableObject } from "../core/utils";

export class Infrastructure extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}