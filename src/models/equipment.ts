import { AssignableObject } from "../core/utils";

export class Equipment extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}