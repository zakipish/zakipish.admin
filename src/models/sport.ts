import { AssignableObject } from "../core/utils";

export class Sport extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}
