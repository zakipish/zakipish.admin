import { AssignableObject } from "../core/utils";

export class PlaygroundSchedule extends AssignableObject {
  id: Number;
  price?: number;
  interval?:[string,string];
  date?:string
}
