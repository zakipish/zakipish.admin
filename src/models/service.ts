import { AssignableObject } from "../core/utils";

export class Service extends AssignableObject {
  id: Number;
  name: string;
  description: string;
  slug: string;
}