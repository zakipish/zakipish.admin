import { AssignableObject } from "../core/utils";

export class Department extends AssignableObject {
  id: Number;
  name: string;
  city_id:Number;
}