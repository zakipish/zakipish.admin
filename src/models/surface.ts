import { AssignableObject } from "../core/utils";

export class Surface extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
}