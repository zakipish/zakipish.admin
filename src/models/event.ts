import { AssignableObject } from "../core/utils";
import { User } from "./user";
import { Club } from "./club";
import { Playground } from "./playground";
import { Sport } from "./sport";
import { GameLevel } from './game-level';
import { Image } from './image';

export enum EventType {
    Meeting = 1,
    Game
}

export enum ParticipationType {
    Possible = 1,
    Definitely,
    Declined,
}

export class Event extends AssignableObject {
    id: Number;
    slug: string;
    description: string;
    club_id: number;
    club: Club;
    playground_id: number;
    playground: Playground;
    sport_id: number;
    sport: Sport;
    game_level_id: number;
    gameLevel: GameLevel;
    chat_disabled: boolean;
    max_players: number;
    min_players: number;
    title: string;
    price: number;
    type: EventType;
    coordinates: string;
    blockSchedule: boolean;
    interval: [string, string];
    date: string;
    age_start: number;
    age_end: number;
    updated_at: string;
    images: Image[];
    address: string;
    users: User[];
    city_id: number;
    place_type: string;

    constructor(args?) {
        super(args)
        if (Array.isArray(args.coordinates)) {
            this.coordinates = `(${args.coordinates[0]}, ${args.coordinates[1]})`
        }
    }
}
