import { AssignableObject } from "../core/utils";

export class Club extends AssignableObject {
  id: Number;
  name: string;
  slug: string;
  description: string;
  event_price: number;
}
