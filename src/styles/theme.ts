import { createMuiTheme } from "@material-ui/core/styles";
// import 'typeface-open-sans';

export const theme = createMuiTheme({
  palette: {
    primary: { 
      main: '#6faa09',
      contrastText: '#ffffff'
    },
    secondary: { 
      main: '#212f30'
    },
    // type: 'dark',
    background: {
      // default: '#ddd',
      // paper: '#e2efec',
    }
  },
  typography: {
    fontFamily: 'Open Sans, Helvetica, Arial, serif',
    fontSize: 12,
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: "0",
        textTransform: 'none',
      }
    },
  MuiPaper:{
      rounded:{
        margin:"auto"
      }
  },
  MuiTableCell:{
      root:{
          fontSize:"0.9rem"
      },
      body:{
        fontSize:"0.9rem"
      }
  },
    // MuiCard: {
    //   root: {
    //     borderRadius: "0",
    //     padding: "20px",
    //     boxShadow: "none",
    //     border: "0",
    //     borderBottom: "2px solid #683a0f"
    //   }
    // },
    // MuiAppBar: {
    //   root: {
    //     backgroundColor: "#fff!important",
    //     boxShadow: "none",
    //     borderBottom: "2px solid #1a1a1a",
    //     color: "#000"
    //   }
    // }
  }
});
