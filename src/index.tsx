import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Router as BaseRouter } from "react-router-dom";


import { MuiThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";

import { store } from "./core/store";
import { history } from "./core/history";
import { theme } from "./styles/theme";
import moment from 'moment'

import Layout from "./layout/layout";

moment.locale('ru')

ReactDOM.render(
  <Provider store={store}>
    <BaseRouter history={history}>
      <React.Fragment>
        <CssBaseline />
        <MuiThemeProvider theme={theme}>
          <Layout />
        </MuiThemeProvider>
      </React.Fragment>
    </BaseRouter>
  </Provider>,
  document.getElementById("root")
);
