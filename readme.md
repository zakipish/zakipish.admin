Setup
=====

.env for development

`API_URL=http://zakipish.local/api`

.env.production for production

`API_URL=http://PRODUCTION-URL/api`

run dev server

`yarn start`

build for production

`yarn build`